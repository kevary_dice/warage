﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
public class GameManager : MonoBehaviourPunCallbacks
{


    public GameHud hud;
    public Transform[] spawnPoints;
    public Config _config;
    public float readyTime, roundTime;
    public int currenCharacter;
    public GameObject loading;
    [HideInInspector]
    public CharacterData _char;
    public Text loadingTxt;
    private void Start()
    {
        loading.SetActive(true);
        _char = _config.CharData[(int)PhotonNetwork.LocalPlayer.CustomProperties["character"]];
        hud.SetupHud(_char);
        Spawn();


        if (PhotonNetwork.PlayerList.Length > 1)
        {
            Debug.Log("можно начинать!");
        }
        else
        {
            Debug.Log("Я один в комнате");
        }
        StartCoroutine(TextLoadingAnim());
    }
    public void Spawn()
    {
        Vector3 spawnPos = spawnPoints[0].position;
        if (PhotonNetwork.IsMasterClient)
            spawnPos = spawnPoints[1].position;

       Transform LocalPalayer = PhotonNetwork.Instantiate(_char.PrefabGame.name, spawnPos, Quaternion.identity).GetComponent<Transform>();
        hud.camContrll.player = LocalPalayer;
        hud.playerControll.player = LocalPalayer;
        
        hud.controll.SetActive(true);
    }

    private IEnumerator TextLoadingAnim()
    {
        loadingTxt.text = "Поиск игроков";

        loadingTxt.text += ".";
        yield return new WaitForSeconds(0.5f);
        loadingTxt.text += ".";
        yield return new WaitForSeconds(0.5f);
        loadingTxt.text += ".";
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(TextLoadingAnim());
    }
}
