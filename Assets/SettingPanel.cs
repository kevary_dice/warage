﻿using UnityEngine;
using UnityEngine.UI;
public class SettingPanel : MonoBehaviour
{
    public Dropdown dropdownLang;
    public Toggle sound, skillPos, graphics;


    private void OnEnable()
    {
        LoadSetting();
    }

    public void LoadSetting()
    {
        dropdownLang.value = PlayerPrefs.GetInt(UserData.Id + "Lang");
        sound.isOn  = PlayerPrefs.GetInt(UserData.Id+ "Sound") == 1 ? true : false;
        skillPos.isOn = PlayerPrefs.GetInt(UserData.Id + "SkilPos") == 1 ? true : false;
        graphics.isOn = PlayerPrefs.GetInt(UserData.Id + "Graphics") == 1 ? true : false;
    }
    public void SaveSetting()
    {
        PlayerPrefs.SetInt(UserData.Id + "Lang", dropdownLang.value);       
        PlayerPrefs.SetInt(UserData.Id + "Sound", sound.isOn ? 1 : 0);
        PlayerPrefs.SetInt(UserData.Id + "SkilPos", skillPos.isOn ? 1 : 0);
        PlayerPrefs.SetInt(UserData.Id + "Graphics", graphics.isOn ? 1 : 0);
    }
}
