﻿

using UnityEngine;

using UnityEngine.UI;

public class TooltypeNeedPoint : MonoBehaviour
{
    [SerializeField] private Text textInfo,btnInfo;


    private void OnEnable()
    {
        btnInfo.text =Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE];
    }

    public void SetText(string text)
    {
        textInfo.text =text;
    }
}
