﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Navigation : MonoBehaviour
{

    public BtnNav[] btnNav;
    [SerializeField] private Text chest, inventory, war, skills, shop;
    [SerializeField] private CurrentCharacter _currentCharacter;
    private void OnEnable()
    {
        _currentCharacter = FindObjectOfType<CurrentCharacter>();
        chest.text=Config.Constant.CHESTS[Config.CURRENT_LANGUAGE];
        inventory.text=Config.Constant.INVENTORY[Config.CURRENT_LANGUAGE];
        war.text=Config.Constant.FIGHT[Config.CURRENT_LANGUAGE];
        skills.text=Config.Constant.SKILLS[Config.CURRENT_LANGUAGE];
        shop.text=Config.Constant.SHOP[Config.CURRENT_LANGUAGE];
        
        Navigatin(2);
        UpdateImage();
    }
    public void Navigatin(int value)
    {
        
        for (int i = 0; i < btnNav.Length; i++)
        {
            RectTransform rt = btnNav[i].icon.GetComponent<RectTransform>();
            if (i != value)
            {
                rt.sizeDelta = new Vector2(130, 130);
                btnNav[i].icon.rectTransform.localPosition = new Vector2(0f, 0);
                btnNav[i].TurnUIMess(true);
                btnNav[i].bg.enabled = false;
                btnNav[i].btnTxt.enabled = false;
                if (btnNav[i].panel != null)
                {
                    btnNav[i].panel.SetActive(false);
                } 
               
            }
            else
            {
                btnNav[i].TurnUIMess(false);
                btnNav[i].panel.SetActive(true);
                rt.sizeDelta = new Vector2(180f, 180f);
                btnNav[i].icon.rectTransform.localPosition = new Vector2(0f, 30);
                btnNav[i].bg.enabled = true;
                btnNav[i].btnTxt.enabled = true;

            }
            

//            if (btnNav[i].isMess)
//            {
//                btnNav[i].mess.SetActive(true);
//            }

            
        }
    }
    
    
    

    public void UpdateImage()
    {
        StartCoroutine(QueryChest("queryTimer",0,UpdateChest));
        UpdateSkills();
        UpdateInventory();
    }

    private void UpdateChest()
    {
        BtnNav chest =
            btnNav[0];
        int test = UserData.ChestTime.ChestTimes.FindAll(x => x == 0).Count;
        if (test>0)
        {
            chest.SetText(test);
            chest.IsMess = true;
        }else
            chest.IsMess = false;
        
    }
    
    private void UpdateInventory()
    { 
        BtnNav inventory =
        btnNav[1];
        
        if (_currentCharacter.CharacterData.CurrentPoint>0)
        {
            inventory.SetText(_currentCharacter.CharacterData.CurrentPoint);
            inventory.IsMess = true;
        }else
            inventory.IsMess = false;
        
    }
    private void UpdateSkills()
    {
        BtnNav skills =
            btnNav[3];
        if (_currentCharacter.CharacterData.CountSkillPoint>0)
        {
            skills.SetText(_currentCharacter.CharacterData.CountSkillPoint);
            skills.IsMess = true;
        }else
            skills.IsMess = false;
    }
  
    
    IEnumerator QueryChest(string action,int _id,UnityAction ac)
    {
        WWWForm form = new WWWForm();
        form.AddField("userId", UserData.Id);
        form.AddField("chestId", _id);
        form.AddField("action", action);
        WWW www = new WWW(Config.url, form);
        yield return www;

        Debug.LogError(www.text);
        UserData.ChestTimeToJson(www.text);
        
        ac.Invoke();
         
    }
}
