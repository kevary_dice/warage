﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class charCellSelect : MonoBehaviour
{
   
    public Image selectBorder, charIcon;
    public int index;
    public Text charName;
    public SelectCharacter selectCharacter;

    

    [SerializeField]
    private GameObject _character;

    public GameObject Character
    {
        get => _character;
        set => _character = value;
    }

    public void Select()
    {
        selectCharacter.ChoseCharacter(index);
    }
}
