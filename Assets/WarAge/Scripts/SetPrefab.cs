﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPrefab : MonoBehaviour
{
    [SerializeField]
    private Player _player;
    private CurrentCharacter _currentCharacter;
    void Start()
    {
        _currentCharacter = FindObjectOfType<CurrentCharacter>();
        Debug.LogError("создается префаб из меню!!!!");
        Instantiate(_currentCharacter.CharacterData.PrefabMenu, _player.GetComponent<Transform>());
    }

    
}
