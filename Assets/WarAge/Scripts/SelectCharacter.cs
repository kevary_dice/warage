﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SelectCharacter : MonoBehaviour
{
    [SerializeField] private Tooltype _tooltype;

    public Tooltype Tooltype
    {
        get => _tooltype;
        set => _tooltype = value;
    }
    [SerializeField] private Transform _characterContainer;
    public GameObject tab0;
    public GameObject tab1;
    public GameObject btnTabs0, btnTabs1;
    public Transform SpawnPiont;
    public Transform btnSelecCharContainer;
    public ButtonSelectChar BuySelectButton;
    public Config _config;
    [SerializeField] private CurrentCharacter _currentCharacter;
    public Transform activeSkillContainer, passiveSkillContainer;
    public GameObject skillCellPrefab;
    public Image typeImage;
    public Sprite melee, magic, shield;
    public Text charNametextAndLvl, infoTextName, infoTextDisc, forceTxt, agility, nouse, armorTxt,
        charTypeText,baseCharacter,information,skills,activSkills,passivSkills;

    
    public void ChoseCharacter(int value)
    {

        
        SelectTabse(0);
        MainMenuManager.CurrenSelectChar = value;
        //проходим по всем строкам контейнера кнопок персонажей
        for (int i = 0; i < btnSelecCharContainer.childCount; i++)
        {
            //проходим по всем иконкам строк контейнера персонажей
            for (int x = 0; x < btnSelecCharContainer.GetChild(i).childCount; x++)
            {
                //если иконка содержит компонетент charSelect мы выключаем ее рамку рамку и вообще все
                if (btnSelecCharContainer.GetChild(i).GetChild(x).GetComponent<charCellSelect>() != null)
                {
                    charCellSelect ccs = btnSelecCharContainer.GetChild(i).GetChild(x).GetComponent<charCellSelect>();
                    ccs.selectBorder.gameObject.SetActive(false);


                    ccs.charName.text = "";

                    // но если ее индекс сооветствует значению иконки по которой мы кликнули, надо включить снова
                    if (ccs.index == value)
                    {
                        ccs.selectBorder.gameObject.SetActive(true);

                        if (_characterContainer.childCount != 0)
                        {
                            Destroy(_characterContainer.GetChild(0).gameObject);
                        }
                        _currentCharacter.CurrentCharacterObject =
                        Instantiate(_config.CharData[MainMenuManager.CurrenSelectChar].PrefabMenu, _characterContainer);
                        //                        Instantiate(_char);
                        ccs.charName.text = _config.CharData[MainMenuManager.CurrenSelectChar].CharNames[Config.CURRENT_LANGUAGE];
                    }
                }
            }
        }

        //удаляем все иконки силов
        for (int i = 0; i < activeSkillContainer.childCount; i++)
        {
            Destroy(activeSkillContainer.GetChild(i).gameObject);
        }

        for (int i = 0; i < passiveSkillContainer.childCount; i++)
        {
            Destroy(passiveSkillContainer.GetChild(i).gameObject);
        }

        //записываем новые
        for (int i = 0; i < _config.CharData[MainMenuManager.CurrenSelectChar].CharSkills.Length; i++)
        {
            GameObject cell;
            if (_config.CharData[MainMenuManager.CurrenSelectChar].CharSkills[i].skillType == SkillData.SkillType.Active)
            {
                cell = Instantiate(skillCellPrefab, activeSkillContainer);
            }
            else
            {
                cell = Instantiate(skillCellPrefab, passiveSkillContainer);
            }

            cell.GetComponent<SkillSelectCharCellPrefab>().index = i;
            cell.GetComponent<SkillSelectCharCellPrefab>().icon.sprite = _config.CharData[MainMenuManager.CurrenSelectChar].CharSkills[i].SkillIcon;
            cell.GetComponent<SkillSelectCharCellPrefab>().SkillData =
                _config.CharData[MainMenuManager.CurrenSelectChar].CharSkills[i];
        }

        // меняем состояние кнопки

        if (_config.CharData[MainMenuManager.CurrenSelectChar].isBuyabled)
            BuySelectButton.ChangeText(Config.Constant.CHOOSE_CHARACTER[Config.CURRENT_LANGUAGE]);
        else
            BuySelectButton.ChangeText(Config.Constant.BUY_FOR[Config.CURRENT_LANGUAGE] + _config.CharData[MainMenuManager.CurrenSelectChar].buyPrice + " " +
                                       _config.CharData[MainMenuManager.CurrenSelectChar].currency);
        typeImage.sprite = _config.CharData[MainMenuManager.CurrenSelectChar].CharTypeIcon;
        
        //записываем параметры данного персонажа
        charNametextAndLvl.text = _config.CharData[MainMenuManager.CurrenSelectChar].CharNames[Config.CURRENT_LANGUAGE] + " " + _config.CharData[MainMenuManager.CurrenSelectChar].lvl +Config.Constant.LVL[Config.CURRENT_LANGUAGE] ;
        infoTextName.text = _config.CharData[MainMenuManager.CurrenSelectChar].CharNames[Config.CURRENT_LANGUAGE];
        infoTextDisc.text = _config.CharData[MainMenuManager.CurrenSelectChar].CharDiscs[Config.CURRENT_LANGUAGE];
        forceTxt.text =Config.Constant.FORCE[Config.CURRENT_LANGUAGE]+" " + _config.CharData[MainMenuManager.CurrenSelectChar].force;
        agility.text =Config.Constant.AGILITY[Config.CURRENT_LANGUAGE]+" "  + _config.CharData[MainMenuManager.CurrenSelectChar].agility;
        nouse.text =Config.Constant.INTELLIGENCE[Config.CURRENT_LANGUAGE]+" "  + _config.CharData[MainMenuManager.CurrenSelectChar].mind;
        armorTxt.text =Config.Constant.ARMOR_SMALL_KEYS[Config.CURRENT_LANGUAGE]+" "  + _config.CharData[MainMenuManager.CurrenSelectChar].armor;
        if (_config.CharData[MainMenuManager.CurrenSelectChar].attackType==CharacterData.AttackType.Magic)
        {
            charTypeText.text = Config.Constant.MAGIC[Config.CURRENT_LANGUAGE];
        }
        else if (_config.CharData[MainMenuManager.CurrenSelectChar].attackType==CharacterData.AttackType.Melee)
        {
            charTypeText.text =Config.Constant.MELEE[Config.CURRENT_LANGUAGE] ;
        }else
        {
            charTypeText.text =Config.Constant.RANGE[Config.CURRENT_LANGUAGE] ;
        }
    }

    private void OnEnable()
    {
        ChoseCharacter(MainMenuManager.CurrenSelectChar);
        if(_currentCharacter.CurrentCharacterObject!=null)
            _currentCharacter.CurrentCharacterObject.SetActive(true);
        baseCharacter.text = Config.Constant.BASE_PARAM[Config.CURRENT_LANGUAGE];
        information.text = Config.Constant.INFORMATION[Config.CURRENT_LANGUAGE];
        skills.text = Config.Constant.SKILLS[Config.CURRENT_LANGUAGE];
        activSkills.text = Config.Constant.ACTIVE_SKILLS[Config.CURRENT_LANGUAGE];
        passivSkills.text = Config.Constant.PASSIVE_SKILLS[Config.CURRENT_LANGUAGE];
    }

    public void SelectTabse(int value)
    {
        if (value == 0)
        {
            tab0.SetActive(true);
            tab1.SetActive(false);
            btnTabs0.transform.GetChild(0).gameObject.SetActive(true);
            btnTabs0.transform.GetChild(1).gameObject.SetActive(true);
            btnTabs0.transform.GetChild(2).gameObject.SetActive(true);
            btnTabs1.transform.GetChild(0).gameObject.SetActive(false);
            btnTabs1.transform.GetChild(1).gameObject.SetActive(false);
            btnTabs1.transform.GetChild(2).gameObject.SetActive(false);
        }
        else
        {
            tab0.SetActive(false);
            tab1.SetActive(true);
            btnTabs0.transform.GetChild(0).gameObject.SetActive(false);
            btnTabs0.transform.GetChild(1).gameObject.SetActive(false);
            btnTabs0.transform.GetChild(2).gameObject.SetActive(false);
            btnTabs1.transform.GetChild(0).gameObject.SetActive(true);
            btnTabs1.transform.GetChild(1).gameObject.SetActive(true);
            btnTabs1.transform.GetChild(2).gameObject.SetActive(true);
        }
    }

    public void NextSelecChar()
    {
        MainMenuManager.CurrenSelectChar++;
        if (MainMenuManager.CurrenSelectChar > _config.CharData.Count - 1)
        {
            MainMenuManager.CurrenSelectChar = 0;
        }

        ChoseCharacter(MainMenuManager.CurrenSelectChar);
    }

    public void BackSelecChar()
    {
        MainMenuManager.CurrenSelectChar--;
        if (MainMenuManager.CurrenSelectChar < 0)
        {
            MainMenuManager.CurrenSelectChar = _config.CharData.Count - 1;
        }

        ChoseCharacter(MainMenuManager.CurrenSelectChar);
    }

}
