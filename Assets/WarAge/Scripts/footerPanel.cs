﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class footerPanel : MonoBehaviour
{

    public Image[] navs;

    private void Start()
    {
        navClick(0);
    }

    public void policy()
    {
        Application.OpenURL("https://www.google.com/");
    }


    public void navClick(int value)
    {
        for (int i = 0; i < navs.Length; i++)
        {
            if(i== value)
            {
                navs[i].transform.GetChild(0).gameObject.SetActive(true);
                navs[i].transform.GetChild(1).gameObject.SetActive(true);
                navs[i].transform.GetChild(2).gameObject.SetActive(true);
            }
            else
            {
                navs[i].transform.GetChild(0).gameObject.SetActive(false);
                navs[i].transform.GetChild(1).gameObject.SetActive(false);
                navs[i].transform.GetChild(2).gameObject.SetActive(false);
            }
        }
    }
}
