﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AcceptBuy : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Text textInfo, textButton;
    private bool _check;

    public void OnPointerClick(PointerEventData eventData)
    {
        gameObject.SetActive(false);
    }

    public void SetText(bool needBuy, string newText = null)
    {
        textButton.text = Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE];
        _check = needBuy;
        if (needBuy)
        {
            textInfo.text = Config.Constant.ACCEPT_BUY[Config.CURRENT_LANGUAGE];
        }
        else if (newText == null)
        {
            textInfo.text = Config.Constant.NEED_FUNDS[Config.CURRENT_LANGUAGE];
        }
        else
            textInfo.text = newText;
    }

    public void OffMe()
    {
        gameObject.SetActive(false);
    }

    public void Purchase()
    {
        if (_check)
        {
            FindObjectOfType<MainMenuManager>().PurchaseChar();
        }
        else

            OffMe();
    }
}