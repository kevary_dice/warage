﻿using System;
using System.Collections;
using UnityEngine;


public class CurrentCharacter : MonoBehaviour
{
    public static CurrentCharacter instance ;
//    [SerializeField]private 
    [SerializeField] private CharacterData _currentCharacter;
    [SerializeField] private GameObject _currentCharacterObject;

    public CharacterData CharacterData
    {
        get => _currentCharacter;
        set => _currentCharacter = value;
    }

    public GameObject CurrentCharacterObject
    {
        get => _currentCharacterObject;
        set => _currentCharacterObject = value;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);

//        StartCoroutine(SaveHP());
    }

 
}