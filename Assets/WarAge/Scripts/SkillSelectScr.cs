﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSelectScr : MonoBehaviour
{

    private Tooltype _tooltype;
    [SerializeField] private SkillData _skillData;
    [SerializeField] private ScrScills _parentTooltype;

    private void Start()
    {
        _parentTooltype = FindObjectOfType<ScrScills>();
//        _tooltype = FindObjectOfType<Tooltype>();
    }
    

    public void OnTooltyp()
    {
        
//        _tooltype = Instantiate(_tooltype,FindObjectOfType<SelectCharacter>().gameObject.transform);
        _parentTooltype.Tooltype.SetParametrs(_skillData);
        _parentTooltype.Tooltype.gameObject.SetActive(true);
    }
}
