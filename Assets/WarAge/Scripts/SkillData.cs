﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "WarAge/NewSkill", fileName = "NewSkill")]
public class SkillData : ScriptableObject
{
    [SerializeField] private CharacterData _character;

    public string SkillName;
    public List<string> SkillNames;
    [Multiline(4)] public string SkillDisc;
    public List<string> SkillDiscs;
    public Sprite SkillIcon;

    public enum SkillType
    {
        Active,
        Passive
    }

    public SkillType skillType;
    public bool isInvestigated;
    public int SkillLevel;

    public float Dmg, UseSpeed, ActTime;
    public int NedeedMP;
    public GameObject SkillPrefab;
    [Header("Дополнительное воздействие")] public bool isbuff;
    public float addMoveSpeed, addSpdAttack, addArrmor, addHp, addMp, maxHp, maxMp, addPAttack, addMAttack;

    [SerializeField] private int _minLvlToLearn;
    
    [SerializeField] private List<SkillData> _skillsBlockMe;
    [SerializeField] private List<SkillData> _skillsPrevsMe;

    public List<SkillData> SkillsBlockMe => _skillsBlockMe;

    public List<SkillData> SkillsPrevsMe => _skillsPrevsMe;

    public bool LvlUp()
    {
        Debug.LogError(_character.CountSkillPoint + " " +
                       _minLvlToLearn + " " +
                       _skillsBlockMe.FindAll(x => x.SkillLevel > 0).Count + " " +
                       (_skillsPrevsMe.FindAll(x => x.SkillLevel > 0).Count + " " + _skillsPrevsMe.Count));

        if (_character.CountSkillPoint > 0 &&
            _minLvlToLearn <= _character.lvl &&
            _skillsBlockMe.FindAll(x => x.SkillLevel > 0).Count == 0 &&
            (_skillsPrevsMe.FindAll(x => x.SkillLevel > 0).Count != 0 || _skillsPrevsMe.Count == 0) &&
            SkillLevel <= 10)
        {
            _character.CountSkillPoint--;
            SkillLevel++;
            if (SkillLevel > 0)
            {
                isInvestigated = true;
            }

            return true;
        }

        return false;
    }

    public bool LvlDown()
    {
        if (SkillLevel > 0)
        {
            _character.CountSkillPoint++;
            SkillLevel--;
            if (SkillLevel == 0)
            {
                isInvestigated = false;
            }

            return true;
        }

        return false;
    }

    public void LvlEquality(int count)
    {
        if (count > SkillLevel)
        {
            LvlUpToI(count - SkillLevel);
        }
        else
            LvlDownToI(SkillLevel - count);
    }

    public void LvlUpToI(int count)
    {
        for (int i = 0; i < count; i++)
        {
            LvlUp();
        }
    }

    public void LvlDownToI(int count)
    {
        for (int i = 0; i < count; i++)
        {
            LvlDown();
        }
    }
}