﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealerChar : MonoBehaviour
{
    public static HealerChar instance;
    [SerializeField] private float timeToHeal = 60f;
    [SerializeField] private float heal = 0.1f;
    [SerializeField] private MainScreen _mainScreen;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);

        StartCoroutine(SaveHP());
    }


    IEnumerator SaveHP()
    {
        if (CurrentCharacter.instance.CharacterData != null)
        {
            if (!PlayerPrefs.HasKey(CurrentCharacter.instance.CharacterData.id+"hp") ||
                CurrentCharacter.instance.CharacterData.hp == CurrentCharacter.instance.CharacterData.maxHp)
            {
                PlayerPrefs.SetInt(CurrentCharacter.instance.CharacterData.id+"hp",
                    (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            }



            int test = (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds -
                       PlayerPrefs.GetInt(CurrentCharacter.instance.CharacterData.id+"hp");

//            Debug.LogError("test " + test);

            if (CurrentCharacter.instance.CharacterData.hp < CurrentCharacter.instance.CharacterData.maxHp &&
                test / timeToHeal > 0)
            {
                CurrentCharacter.instance.CharacterData.RestoreHp(
                    Mathf.RoundToInt(CurrentCharacter.instance.CharacterData.maxHp * heal * test / timeToHeal));
                _mainScreen.OnEnable();
            }
            
//            Debug.LogError("heal on "+
//                           Mathf.RoundToInt(CurrentCharacter.instance.CharacterData.maxHp * heal * test / timeToHeal) );

            if (CurrentCharacter.instance.CharacterData.hp < CurrentCharacter.instance.CharacterData.maxHp)
            {
                PlayerPrefs.SetInt(CurrentCharacter.instance.CharacterData.id+"hp",
                    (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            }
        }

        if (CurrentCharacter.instance.CharacterData != null)
        {
            if (!PlayerPrefs.HasKey(CurrentCharacter.instance.CharacterData.id+"mp") ||
                CurrentCharacter.instance.CharacterData.mp == CurrentCharacter.instance.CharacterData.maxMp)
            {
                PlayerPrefs.SetInt(CurrentCharacter.instance.CharacterData.id+"mp",
                    (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            }



            int test = (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds -
                       PlayerPrefs.GetInt(CurrentCharacter.instance.CharacterData.id+"mp");

//            Debug.LogError("test " + test);

            if (CurrentCharacter.instance.CharacterData.mp < CurrentCharacter.instance.CharacterData.maxMp &&
                test / timeToHeal > 0)
            {
                CurrentCharacter.instance.CharacterData.RestoreMp(
                    Mathf.RoundToInt(CurrentCharacter.instance.CharacterData.maxMp * heal * test / timeToHeal));
                _mainScreen.OnEnable();
            }
            
//            Debug.LogError("heal on "+
//                           Mathf.RoundToInt(CurrentCharacter.instance.CharacterData.maxMp * heal * test / timeToHeal) );

            if (CurrentCharacter.instance.CharacterData.mp < CurrentCharacter.instance.CharacterData.maxMp)
            {
                PlayerPrefs.SetInt(CurrentCharacter.instance.CharacterData.id+"mp",
                    (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            }
        }
        
        
        yield return new WaitForSeconds(timeToHeal);

        StartCoroutine(SaveHP());
    }
}