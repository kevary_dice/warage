﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MainMenuManager : MonoBehaviourPunCallbacks
{
    private bool needShowUserName;
    public Text duelTxt, goldTxt, cristalTxt;

    public UserNamePanel UserNamePanel;

    public GameObject MainScreenPanel,panelLoading;

    [SerializeField] private Purchase _purchase;
    private GameObject parentCharacter;
    [SerializeField] private AcceptBuy _acceptBuy;

    [SerializeField] private CharIdLvlList charIdLvls;

    [SerializeField] private CurrentCharacter _currentCharacter;

    public CurrentCharacter CurrentCharacter
    {
        get => _currentCharacter;
        set => _currentCharacter = value;
    }

    public Config _config;
    public static int CurrenSelectChar = 0;


    public SelectCharacter selecCharacterScr;


    public Scrollbar sb;
    [SerializeField] private int _costGame = -1;


    private GameObject _char;
    bool check = false;

    public void CheckLanguageNotNull()
    {
        foreach (var item in _config.AllItem)
        {
            if (item.itemNames.Count == 0)
            {
                item.itemNames.Add(item.ItemName);
                item.itemDiscrs.Add(item.ItemDiscr);
            }
        }

        foreach (var character in _config.CharData)
        {
            if (character.CharNames.Count == 0)
            {
                character.CharNames.Add(character.CharName);
                character.CharDiscs.Add(character.CharDisc);
            }

            foreach (var skills in character.CharSkills)
            {
                if (skills.SkillNames.Count == 0)
                {
                    skills.SkillNames.Add(skills.SkillName);
                    skills.SkillDiscs.Add(skills.SkillDisc);
                }
            }
        }
    }

    public void LoadJsonLanguage()
    {
        string jsonString;

        //#if UNITY_STANDALONE || UNITY_EDITOR
        jsonString = File.ReadAllText(Application.dataPath + Constant.pathToJson);
        //#endif
        //       
        //#if UNITY_ANDROID
        //StreamReader stream = new StreamReader("jar:file://" + Application.dataPath + "!/assets"+"/Language.json");
        //        jsonString = stream.ReadToEnd();
        ////         jsonString =  File.ReadAllText("jar:file://" + Application.dataPath + "!/assets"+"/Language.json");
        //#endif

        Config.Constant = JsonUtility.FromJson<Constant>(jsonString);

    }
    private void Awake()
    {
        // LoadJsonLanguage();
        CheckLanguageNotNull();
        charIdLvls = new CharIdLvlList();
        //        foreach (var VARIABLE in _config.AllItem)
        //        {
        //            VARIABLE.ItemSellPrice *= -1;
        //        }

        foreach (var VARIABLE in _config.CharData)
        {
            VARIABLE.isBuyabled = false;
        }

        int index = 0;
        for (int i = 0; i < selecCharacterScr.btnSelecCharContainer.childCount; i++)
        {
            for (int x = 0; x < selecCharacterScr.btnSelecCharContainer.GetChild(i).childCount; x++)
            {
                if (selecCharacterScr.btnSelecCharContainer.GetChild(i).GetChild(x).GetComponent<charCellSelect>() !=
                    null)
                {
                    charCellSelect ccs = selecCharacterScr.btnSelecCharContainer.GetChild(i).GetChild(x)
                        .GetComponent<charCellSelect>();
                    ccs.charIcon.sprite = _config.CharData[index].CharIcon;
                    ccs.index = index;
                    ccs.selectCharacter = selecCharacterScr;
                    index++;
                }
            }
        }

        //        StartCoroutine(PostChar());
    }
    
    
    private void Start()
    {
//                UserData.Id = "Ww2YHZPU7GSJlcSVQ840i7khJI23";
//        //        //  UserData.Hiro = "{\"CharIdLvls\":[{\"id\":5,\"lvl\":0,\"current\":true}]}";
//        //        UserData.Hiro = "{\"CharIdLvls\":[{\"id\":0,\"lvl\":1,\"current\":true,\"itemsId\":[170,196]}]}";
//                UserData.Cristal = 100;
//                UserData.Gold = 100000;
//                UserData.Hiro =
//                    "";
////                    "{\"CharIdLvls\":[{\"id\":1,\"exp\":200,\"current\":false,\"skills\":{\"SkillsLvlUp\":[{\"skillId\":0,\"skillLvl\":3},{\"skillId\":1,\"skillLvl\":0},{\"skillId\":2,\"skillLvl\":0},{\"skillId\":3,\"skillLvl\":0},{\"skillId\":4,\"skillLvl\":0},{\"skillId\":5,\"skillLvl\":0},{\"skillId\":6,\"skillLvl\":0},{\"skillId\":7,\"skillLvl\":0},{\"skillId\":8,\"skillLvl\":0},{\"skillId\":9,\"skillLvl\":0}]},\"point\":{\"currentPoint\":5,\"pointAgility\":26.0,\"pointStrenge\":18.0,\"pointInteligence\":6.0},\"itemsId\":[]},{\"id\":0,\"lvl\":1,\"current\":true,\"skills\":{\"SkillsLvlUp\":[{\"skillId\":0,\"skillLvl\":4},{\"skillId\":1,\"skillLvl\":0},{\"skillId\":2,\"skillLvl\":0},{\"skillId\":3,\"skillLvl\":0},{\"skillId\":4,\"skillLvl\":0},{\"skillId\":5,\"skillLvl\":0},{\"skillId\":6,\"skillLvl\":0},{\"skillId\":7,\"skillLvl\":0},{\"skillId\":8,\"skillLvl\":0},{\"skillId\":9,\"skillLvl\":0}]},\"point\":{\"currentPoint\":5,\"pointAgility\":23.0,\"pointStrenge\":17.0,\"pointInteligence\":3.0},\"itemsId\":[],\"itemUpgrade\":{\"inteligence\":0.0,\"agility\":0.0,\"force\":0.0,\"pAtack\":42.0,\"mAtack\":0.0,\"armor\":125.0,\"spdAttack\":7.0,\"speed\":15.0,\"maxMp\":0.0,\"maxHp\":28.0},\"pointUpgrade\":{\"inteligence\":0,\"agility\":0,\"force\":0}}]}";
//        //        Debug.LogError(JsonUtility.ToJson(UserData.Inventory));
//                UserData.DuelJson = "{\"duel\":[20,30]}";
//                UserData.InventoryToJson(null);
        inicializ();
    }

    public void inicializ()
    {
        duelTxt.text = UserData.CurrentDuel + "\\" + UserData.MaxDuel;
        goldTxt.text = UserData.Gold.ToString();
        cristalTxt.text = UserData.Cristal.ToString();

        CharIdLvlList countChar = new CharIdLvlList();
        if (UserData.Hiro.Length != 0)
            countChar = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);


        if (countChar.CharIdLvls.Count > 0)
        {
            needShowUserName = false;
            SetAcountHiro(countChar);

            MainScreenPanel.SetActive(true);
        }
        else
        {
            needShowUserName = true;
            selecCharacterScr.gameObject.SetActive(true);
            selecCharacterScr.ChoseCharacter(0);
        }
        
    }

    
    public void SetAcountHiro(CharIdLvlList charIdLvlList)
    {
        foreach (var VARIABLE in charIdLvlList.CharIdLvls)
        {
            _config.CharData[VARIABLE.id].CurrentExp = VARIABLE.exp;
            _config.CharData[VARIABLE.id].isBuyabled = true;
            if (VARIABLE.current)
            {
                _currentCharacter.CharacterData = _config.CharData[VARIABLE.id];
                CurrenSelectChar = VARIABLE.id;
            }

            foreach (var Skills in VARIABLE.skills.SkillsLvlUp)
            {
                _config.CharData[VARIABLE.id].CharSkills[Skills.skillId].LvlEquality(Skills.skillLvl);
            }

            _config.CharData[VARIABLE.id].PointUpgrade = VARIABLE.pointUpgrade;
            
            _config.CharData[VARIABLE.id].ItemUpgrade = VARIABLE.itemUpgrade;
        }
    }


    public void ScrollContorl(float value)
    {
        sb.value = value;
    } //TODO перенести в сундуки так ка киспользуется только для скрину сундуки

    public void GoGame()
    {
        StartCoroutine(StartGame());
    }

    //TODO  Надо как то перенести в SelecCharacer

    public void ChooseChar()
    {
        if (_config.CharData[CurrenSelectChar].isBuyabled)
        {
            charIdLvls.CharIdLvls.Clear();
            if (!string.IsNullOrEmpty(UserData.Hiro))
            {
                charIdLvls = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
            }


            for (int i = 0; i < _config.CharData.Count; i++)
            {
                if (_config.CharData[i].isBuyabled)
                {
                    CharIdLvl charIdLvl = new CharIdLvl()
                    { id = i, exp = _config.CharData[i].CurrentExp, current = false };
                    if (i == CurrenSelectChar)

                        charIdLvl.current = true;
                    charIdLvl.point = new PointLvlUp()
                    {
                        currentPoint = _config.CharData[i].currentPoint,
                        pointStrenge = _config.CharData[i].force,
                        pointAgility = _config.CharData[i].agility,
                        pointInteligence = _config.CharData[i].mind
                    };
                    charIdLvl.pointUpgrade = _config.CharData[i].PointUpgrade;
                    charIdLvl.itemUpgrade = _config.CharData[i].ItemUpgrade;

                    if (charIdLvls.CharIdLvls != null)
                    {
                        CharIdLvl _charIdLvl = charIdLvls.CharIdLvls.Find(x => x.id == i);


                        if (_charIdLvl != null)
                        {
                            if (i == CurrenSelectChar)
                            {
                                _charIdLvl.current = true;
                            }
                            else
                            {
                                _charIdLvl.current = false;
                            }
                        }
                        else
                            charIdLvls.CharIdLvls.Add(charIdLvl);
                    }
                    else
                        charIdLvls.CharIdLvls.Add(charIdLvl);
                }
            }


            string jsonCharIdLvls = JsonUtility.ToJson(charIdLvls);

            _currentCharacter.CharacterData = _config.CharData[CurrenSelectChar];
            UserData.Hiro = jsonCharIdLvls;

            StartCoroutine(SendChar(jsonCharIdLvls));
            selecCharacterScr.gameObject.SetActive(false);


            //Удаление персонажа 
            _currentCharacter.CurrentCharacterObject.SetActive(false);

            if (needShowUserName)
            {
                needShowUserName = false;
                UserNamePanel.gameObject.SetActive(true);
            }
            else
            {
                //                StartCoroutine(LoginCar(UserData.Id, UserData.UserName, UserData.Email));
                MainScreenPanel.SetActive(true);
            }
        }
        else
        {
            _acceptBuy.gameObject.SetActive(true);

            _acceptBuy.SetText(_purchase.CheckPurchase(_config.CharData[CurrenSelectChar].currency,
                _config.CharData[CurrenSelectChar].buyPrice));
        }
    }

    public void SetCharactersParametrs()
    {
        //Повышение уровня до 1
        //        CharData[CurrenSelectChar].CurrentExp = CharData[CurrenSelectChar].expForLvl[1];
        // 1 силы = x30 MaxHp и x5 pAttack
        //        _config.CharData[CurrenSelectChar].pAttack = _config.CharData[CurrenSelectChar].force * 5f;
        //        _config.CharData[CurrenSelectChar].maxHp = Convert.ToInt32(_config.CharData[CurrenSelectChar].force * 30f);
        _config.CharData[CurrenSelectChar].hp = _config.CharData[CurrenSelectChar].maxHp;
        // 1 интелект = x5 mAttack и =30 maxMp
        //        _config.CharData[CurrenSelectChar].mAttack = _config.CharData[CurrenSelectChar].mind * 5f;
        //        _config.CharData[CurrenSelectChar].maxMp = Convert.ToInt32(_config.CharData[CurrenSelectChar].mind * 30f);
        _config.CharData[CurrenSelectChar].mp = _config.CharData[CurrenSelectChar].maxMp;
        // 1 ловкость = +0.5 moveSpeed и moveAttack 0.5
        //        _config.CharData[CurrenSelectChar].spdAttack = _config.CharData[CurrenSelectChar].agility * 0.5f;
        //        _config.CharData[CurrenSelectChar].moveSpeed = _config.CharData[CurrenSelectChar].agility * 0.5f;
        //1 уровень 5 поинтов скилов
        //        _config.CharData[CurrenSelectChar].CountSkillPoint = _config.CharData[CurrenSelectChar].lvl * 5;
        //1 уровень 5 поинтов 
        //        _config.CharData[CurrenSelectChar].currentPoint = _config.CharData[CurrenSelectChar].lvl * 5;
    }

    public void SetName()
    {
        StartCoroutine(SendName(UserNamePanel.GetName()));
        UserData.UserName = UserNamePanel.GetName();

        UserNamePanel.gameObject.SetActive(false);
        MainScreenPanel.SetActive(true);
    }

    //TODO обработчик покупки  ------------ добавить 2 входных параметра СТОИМОСТЬ И РАСХОДНИКИ!
    public bool PurchaseChar()
    {
        
        _acceptBuy.gameObject.SetActive(false);
        if (_purchase.PurchaseReturn(_config.CharData[CurrenSelectChar].currency,
            _config.CharData[CurrenSelectChar].buyPrice))
        {
            _config.CharData[CurrenSelectChar].Purchase();
            selecCharacterScr.BuySelectButton.ChangeText("Выбрать персонажа");

            goldTxt.text = UserData.Gold.ToString();
        
            SetCharactersParametrs();

            if (_currentCharacter.CharacterData == null)
            {
                ChooseChar();
            }
            _acceptBuy.gameObject.SetActive(true);
            _acceptBuy.SetText(false,Config.Constant.SUCCESS_BUY[Config.CURRENT_LANGUAGE]);
        }

        return false;
    }


    //--------------------------------------------------------


    //TODO обработчик ввода имени || возможно перенести скрипт на панель имени 


    public IEnumerator PostChar()
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("action", "postChar");
        WWW www = new WWW(Config.url, form);
        yield return www;
        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        Debug.LogError(www.text);
        charIdLvls = JsonUtility.FromJson<CharIdLvlList>(www.text);

        foreach (var VARIABLE in charIdLvls.CharIdLvls)
        {
            _config.CharData[VARIABLE.id].lvl = VARIABLE.exp;
            _config.CharData[VARIABLE.id].isBuyabled = true;

            if (VARIABLE.current)
                _currentCharacter.CharacterData = _config.CharData[VARIABLE.id];
        }
        panelLoading.SetActive(false);
    }

    public IEnumerator SendChar(string json)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }

    public IEnumerator SendName(string json)
    {
       
        Debug.LogError(json);
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("name", json);
        form.AddField("action", "setName");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }

    //===================================================
    public IEnumerator UpdateDuel(int value)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("userId", UserData.Id);
        form.AddField("value", value);
        form.AddField("action", "sendDuel");
        WWW www = new WWW(Config.url, form);
        yield return www;
//        Debug.LogError(www.text);
        Duel _duel = JsonUtility.FromJson<Duel>(www.text);
        Debug.LogError(_duel.duel[0]);
        if (_duel.duel[0] >= 0)
        {
            UserData.DuelJson = www.text;
            duelTxt.text = UserData.CurrentDuel + "\\" + UserData.MaxDuel;
            check = true;
        }
        else
        {
            check = false;
        }
        panelLoading.SetActive(false);
    }

    public IEnumerator StartGame()
    {
        panelLoading.SetActive(true);
        yield return StartCoroutine(UpdateDuel(_costGame));
        if (check)
        {
            check = false;
            // SceneManager.LoadScene(2);
            Connect();
        }
        else Debug.LogError("Не не не");
        panelLoading.SetActive(false);
    }


    public int GetCurrenMap(int lvl)
    {
        int currenMap = lvl / 5;
        if (currenMap > _config.AllMaps.Length - 1)
            currenMap = _config.AllMaps.Length - 1;

        return currenMap;

    }

    public void Connect()
    {
        PhotonNetwork.NickName = UserData.UserName;
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 2;
        options.CustomRoomProperties = new Hashtable();
        options.CustomRoomProperties.Add("map", GetCurrenMap(CurrentCharacter.CharacterData.lvl));

        Hashtable PlayerProp = new Hashtable();
        PlayerProp.Add("character", CurrenSelectChar);
        PlayerProp.Add("lvl", CurrentCharacter.CharacterData.lvl);
        PhotonNetwork.LocalPlayer.SetCustomProperties(PlayerProp);

        PhotonNetwork.JoinRandomRoom(options.CustomRoomProperties, options.MaxPlayers);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 2;
        options.CustomRoomProperties = new Hashtable();
        options.CustomRoomProperties.Add("map", GetCurrenMap(CurrentCharacter.CharacterData.lvl));

        Hashtable PlayerProp = new Hashtable();
        PlayerProp.Add("character", CurrenSelectChar);
        PlayerProp.Add("lvl", CurrentCharacter.CharacterData.lvl);
        PhotonNetwork.LocalPlayer.SetCustomProperties(PlayerProp);


        PhotonNetwork.CreateRoom(UserData.Id, options, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        SceneManager.LoadScene(_config.AllMaps[(int)PhotonNetwork.CurrentRoom.CustomProperties["map"]].memberScene);
    }
}