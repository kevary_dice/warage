﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Config : ScriptableObject
{
    public static Constant Constant;
    public static int CURRENT_LANGUAGE = 0;
    public List<Color> rarityColor;
    public static string url="https://roketstars.ru/WarAge/launcher.php";
    public List<int> expForLvl;
    public List<CharacterData> CharData;
    public Maps[] AllMaps;
    public List<Item> AllItem;

    public void LoadLanguage()
    {
        
    }

   
}

[System.Serializable]
public class Maps
{
    public string mapName;
    public int memberScene;
    public Sprite mapPicture;




}