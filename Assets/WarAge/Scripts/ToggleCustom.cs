﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ToggleCustom : MonoBehaviour
{
    public Animator anim;
    public Toggle tgl;
    public Text txt;
    public string isOffTxt, isOnTxt;   
    public Color onColor, offColor;

    public void ChangeTxt()
    {
        if (tgl.isOn)
        {
            txt.text = isOnTxt;
        }
        else
        {
            txt.text = isOffTxt;
        }
    }
    

    public void Used()
    {
        if (tgl.isOn)
        {
            anim.Play("ON");
            txt.text = isOnTxt;
            txt.color = onColor;
        }
        else
        {
            txt.text = isOffTxt;
            anim.Play("OFF");
            txt.color = offColor;
        }
            
    }

}
