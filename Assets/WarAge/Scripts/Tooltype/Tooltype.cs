﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Tooltype : MonoBehaviour
{
    [SerializeField] private Text _skillName;
    [SerializeField] private Text _skillDiscr;
    [SerializeField] private Image _imageSkill;
    [SerializeField] private Text _damageText;
    [SerializeField] private Text _reuseText;
    [SerializeField] private Text _stunText;
    [SerializeField] private Text _needMpText,learnBtn;
    private SkillData _skillData;


    public void OffToolType()
    {
        gameObject.SetActive(false);
    }

    public void SetParametrs(SkillData _skillData)
    {
        _skillName.text = _skillData.SkillNames[Config.CURRENT_LANGUAGE];
        _imageSkill.sprite = _skillData.SkillIcon;
        _skillDiscr.text = _skillData.SkillDiscs[Config.CURRENT_LANGUAGE];
        _damageText.text =Config.Constant.DAMAGE[Config.CURRENT_LANGUAGE]+ " <b><color=orange>" + _skillData.Dmg + "</color></b>";
        _reuseText.text =Config.Constant.REUSE[Config.CURRENT_LANGUAGE]+ " <color=#2DD9FF>" + _skillData.UseSpeed + " сек. " + "</color>";
        _stunText.text =Config.Constant.STUN_ENEMY_FOR[Config.CURRENT_LANGUAGE]+ " <color=#00DB1E>" + _skillData.ActTime + " .</color>";
        _needMpText.text =Config.Constant.NEED[Config.CURRENT_LANGUAGE]+ " <color=#00DB1E>"+_skillData.NedeedMP+".</color>";
        learnBtn.text = Config.Constant.LEARN[Config.CURRENT_LANGUAGE];
    }
}
