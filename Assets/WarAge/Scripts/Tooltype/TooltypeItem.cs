﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TooltypeItem : MonoBehaviour
{
    [SerializeField] private ShopPanel _shopPanel;

    [SerializeField] private Image _itemIconImage,itemBg;
    [SerializeField] private Text _itemDiscriptionText;
    [SerializeField] private Text _itemTypeText;
    [SerializeField] private Text _itemNameText;
    [SerializeField] private Button btn;
    [SerializeField] private Text textBtn;
    [SerializeField] private int count;
[SerializeField]
    private ItemPrefab currentItem;

    [SerializeField] private Purchase _purchase;

    public void Purchase()
    {
        StartCoroutine(test());
    }

    public void OffToolType()
    {
        gameObject.SetActive(false);
    }

    public void SetParametrs(ItemPrefab _item,int _count=1)
    {
        count = _count;
        if (_shopPanel.TextName.text==Config.Constant.SEND[Config.CURRENT_LANGUAGE] )
        {
            textBtn.text = Config.Constant.SELL[Config.CURRENT_LANGUAGE];
        }
        else
            textBtn.text = Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE];

        currentItem = _item;
        
        itemBg.color = currentItem.RarityBg.color;
        _itemIconImage.sprite = _item.CurrentItem.ItemIcon;
        _itemDiscriptionText.text = _item.CurrentItem.itemDiscrs[Config.CURRENT_LANGUAGE];
        _itemNameText.text = _item.CurrentItem.itemNames[Config.CURRENT_LANGUAGE];
    }

    IEnumerator test()
    {
        btn.interactable = false;
        yield return _shopPanel.Purchase(currentItem,count);
        btn.interactable = true;
        OffToolType();
    }
}