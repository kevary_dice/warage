﻿using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class TooltypeInventory : MonoBehaviour
{
    [SerializeField] private Text title, disc, type, mAtack, speedAtack, maxMP, needLvl;
    [SerializeField, ReadOnly(true)] private bool isInventory;
    [SerializeField] private Item currentItem;
    [SerializeField] private ScrnInvenory _scrnInvenory;
    [SerializeField] private Text btnText,btnOnShop;
    [SerializeField] private bool isConsumbles;
    [SerializeField] private ItemPrefab _itemPrefab;
    public void SetParametrs(Item item, bool _isInventory)
    {
       _itemPrefab.SetIcon(item);
        isConsumbles = false;
        currentItem = item;
        isInventory = _isInventory;
        title.text = item.itemNames[Config.CURRENT_LANGUAGE];
        disc.text = item.itemDiscrs[Config.CURRENT_LANGUAGE];
        type.text = item.itemTyepe.ToString();
        if (btnOnShop!=null)
        {
            btnOnShop.text =Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE] ;
        }else
        if (_isInventory)
        {
            btnText.text =Config.Constant.PUT_ON[Config.CURRENT_LANGUAGE] ;
        }
        else
        {
            btnText.text = Config.Constant.PUT_OFF[Config.CURRENT_LANGUAGE]; 
        }
        try
        {
            ItemWeapon weapon = item as ItemWeapon;
            mAtack.text = weapon.addMAttack.ToString();
            speedAtack.text = weapon.AddSpdAttack.ToString();
            maxMP.text = weapon.addMaxMp.ToString();
            needLvl.text = weapon.NeededLevel.ToString();
        }
        catch
        {
            try
            {
                ItemEquip equip = item as ItemEquip;
                mAtack.text = equip.addMAttack.ToString();
                speedAtack.text = equip.AddSpdAttack.ToString();
                maxMP.text = equip.addMaxMp.ToString();
                needLvl.text = equip.NeededLevel.ToString();
            }
            catch
            {
                if (item.itemTyepe == Item.ItemType.Сonsumables)
                {
                    isConsumbles = true;
                    btnText.text =Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE]  ;
                }
                else
                    btnText.text =Config.Constant.CLOSE[Config.CURRENT_LANGUAGE] ;

                mAtack.text = "";
                speedAtack.text = "";
                maxMP.text = "";
                needLvl.text = "";
            }
        }
    }

    public void ClickButton()
    {
        
        if (isConsumbles)
        {
            AplyHp();
        }else if (isInventory)
            PutOnItem();
        else 
        {
            TakeOffItem();
        }
      
    }

    private void PutOnItem()
    {
        _scrnInvenory.PanelLoading.SetActive(true);
        Debug.LogError("PutOn");
        _scrnInvenory.PutItem(currentItem, true);
    }

    private void TakeOffItem()
    { 
        _scrnInvenory.PanelLoading.SetActive(true);
        Debug.LogError("PutOff");
        _scrnInvenory.PutItem(currentItem, false);
    }

    private void AplyHp()
    {
        Debug.LogError("Consumblya");
        CurrentCharacter character = FindObjectOfType<CurrentCharacter>();
        
        
        Inventory inventory = new Inventory()
        {
            itemId = _scrnInvenory.Config.AllItem.IndexOf(
                _scrnInvenory.Config.AllItem.Find(x => x.ItemName == currentItem.ItemName)),
            ItemCount = 1
        };
        
        if (currentItem.ItemName == "Банка жизни")
        {
            character.CharacterData.RestoreHp(character.CharacterData.maxHp / 2);
            
            
        }
        else
        {
            character.CharacterData.RestoreMp(character.CharacterData.maxMp / 2);
        }

        _scrnInvenory.HeapPotion(inventory);
        
    }
    
   
}