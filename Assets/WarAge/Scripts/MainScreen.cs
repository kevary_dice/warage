﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainScreen : MonoBehaviour
{
    [SerializeField] private Image _avatarChar;
    [SerializeField] private Text _hp, _mp, _exp, _lvl,inFight,camp,duel;
    [SerializeField] private Slider _hpSlider, _mpSlider, _expSlider;
    [SerializeField] private GameObject lvlUp;
    public Image currenMapImg;
    public Text currenMapTxt;
    public MainMenuManager mmm;

    // Надо переделать onEnable На простой метод и выполнять когда надо
    public void OnEnable()
    {
        
        inFight.text = Config.Constant.GAME[Config.CURRENT_LANGUAGE];
        camp.text = Config.Constant.TRAIN_CAMP[Config.CURRENT_LANGUAGE];

        var _currentChar = FindObjectOfType<CurrentCharacter>();
        _avatarChar.sprite = _currentChar.CharacterData.CharIcon;

        _hp.text = _currentChar.CharacterData.hp + "\\" + _currentChar.CharacterData.maxHp;
        SetSliderSettings(_hpSlider, _currentChar.CharacterData.hp, _currentChar.CharacterData.maxHp);

        _mp.text = _currentChar.CharacterData.mp + "\\" + _currentChar.CharacterData.maxMp;
        SetSliderSettings(_mpSlider, _currentChar.CharacterData.mp, _currentChar.CharacterData.maxMp);

        _exp.text = _currentChar.CharacterData.CurrentExp + "\\" +
                    _currentChar.CharacterData.config.expForLvl[_currentChar.CharacterData.lvl];
        SetSliderSettings(_expSlider, _currentChar.CharacterData.CurrentExp,
            _currentChar.CharacterData.config.expForLvl[_currentChar.CharacterData.lvl],
            _currentChar.CharacterData.config.expForLvl[_currentChar.CharacterData.lvl - 1]);

        _lvl.text = _currentChar.CharacterData.lvl.ToString();

        currenMapImg.sprite = mmm._config.AllMaps[mmm.GetCurrenMap(_currentChar.CharacterData.lvl)].mapPicture;
        currenMapTxt.text = mmm._config.AllMaps[mmm.GetCurrenMap(_currentChar.CharacterData.lvl)].mapName;
        CheckChar();
        StartCoroutine(UpdateDuel(0));
    }
    
    public void CheckChar()
    {
        PointLvlUp _pointLvlUp =
            JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro).CharIdLvls.Find(x => x.current).point;
        if (_pointLvlUp.currentPoint > 0)
        {
            lvlUp.SetActive(true);
        }
        else
        {
            lvlUp.SetActive(false);
        }
    }

    public void SetSliderSettings(Slider _slider, int _currentValue, int _maxValue)
    {
        _slider.maxValue = _maxValue;
        _slider.value = _currentValue;
    }

    public void SetSliderSettings(Slider _slider, int _currentValue, int _maxValue, int _minValue)
    {
        _slider.minValue = _minValue;
        _slider.maxValue = _maxValue;
        _slider.value = _currentValue;
    }
    
    
    public IEnumerator UpdateDuel(int value)
    {
//        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("userId", UserData.Id);
        form.AddField("value", value);
        form.AddField("action", "sendDuel");
        WWW www = new WWW(Config.url, form);
        yield return www;
//        Debug.LogError(www.text);
        Duel _duel = JsonUtility.FromJson<Duel>(www.text);
//        Debug.LogError(_duel.duel[0]);
        if (_duel.duel[0] >= 0)
        {
            UserData.DuelJson = www.text;
            duel.text = UserData.CurrentDuel + "\\" + UserData.MaxDuel;
//            check = true;
        }
        else
        {
//            check = false;
        }
//        panelLoading.SetActive(false);
    }
}