﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UserData
{
    private static string id;
    private static string email;
    private static string userName;
    private static string duelJson;
    private static int maxDuel;
    private static int currentDuel;
    private static int gold;
    private static int cristal;
    private static string hiro;
    private static string otherData;
    private static string index;
    private static ChestTime chestTime;
    private static Inventories inventory = new Inventories();

    public static string Index
    {
        get => index;
        set => index = value;
    }

    public static Inventories Inventory
    {
        get => inventory;
        set => inventory = value;
    }

    public static void AddInventory(Inventory item)
    {
        try
        {
            inventory.items.Find(x => x.itemId == item.itemId).ItemCount+=item.ItemCount;
        }
        catch (Exception e)
        {
            inventory.items.Add(item);
        }
    }

    public static void RemoveItem(Inventory item)
    {
        
            inventory.items.Find(x => x.itemId == item.itemId).ItemCount-=item.ItemCount;
            if ((inventory.items.Find(x => x.itemId == item.itemId).ItemCount) <= 0)
            {
                inventory.items.Remove(inventory.items.Find(x => x.itemId == item.itemId));
            }
       
    }

    public static void InventoryToJson(string value)
    {
        if (string.IsNullOrEmpty(value) ||value.Length==1)
        {
            value = "{\"items\":[]}";
        }
     
//        Debug.LogError(value.Length+" "+value);
        Inventory = JsonUtility.FromJson<Inventories>(value);
    }


    public static int MaxDuel => maxDuel;

    public static int CurrentDuel => currentDuel;

    public static string Id
    {
        get => id;
        set => id = value;
    }

    public static string Email
    {
        get => email;
        set => email = value;
    }

    public static string UserName
    {
        get => userName;
        set => userName = value;
    }

    public static string DuelJson
    {
        get => duelJson;
        set
        {
            duelJson = value;
            Duel _duel = JsonUtility.FromJson<Duel>(value);
            currentDuel = _duel.duel[0];
            maxDuel = _duel.duel[1];
        }
    }

    public static int Gold
    {
        get => gold;
        set => gold = value;
    }

    public static int Cristal
    {
        get => cristal;
        set => cristal = value;
    }

    public static string Hiro
    {
        get => hiro;
        set => hiro = value;
    }


    public static string OtherData
    {
        get => otherData;
        set => otherData = value;
    }

    public static ChestTime ChestTime
    {
        get => chestTime;
    }


    public static void ChestTimeToJson(string value)
    {
        chestTime = JsonUtility.FromJson<ChestTime>(value);
    }
}

[Serializable]
public class Inventory
{
    public int itemId;
    public int ItemCount;

    public int ItemId
    {
        get => itemId;
        set => itemId = value;
    }
}

[Serializable]
public class Inventories
{
    public List<Inventory> items;

    public Inventories()
    {
        items = new List<Inventory>();
    }
}

public class Duel
{
    public List<int> duel;

    public Duel()
    {
        duel = new List<int>();
    }
}