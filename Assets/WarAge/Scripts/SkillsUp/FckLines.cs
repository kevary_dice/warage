﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FckLines : MonoBehaviour
{
    [SerializeField] private Sprite _activ, _nonActiv;
    [SerializeField] private SkillData _startSkills;

    private void Awake()
    {
        _startSkills = transform.parent.GetComponent<CellScill>().SkillData;
    }

    public void ChangeOnActiv()
    {
       
        
            GetComponent<Image>().sprite = _activ;
    }

    public void ChangeOnNonActiv()
    {
        GetComponent<Image>().sprite = _nonActiv;
    }
}