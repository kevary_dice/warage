﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellScill : MonoBehaviour
{
    [SerializeField] private Image _skillIcon;
    private CurrentCharacter _currentCharacter;
    [SerializeField] private List<FckLines> _lines;
    [SerializeField] private Text _currentLvlText;

    public int CurrentLvlText
    {
        set
        {
            if (value == 0)
            {
                _currentLvlText.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                _currentLvlText.transform.parent.gameObject.SetActive(true);
                _currentLvlText.text = value.ToString();
            }
        }
    }

    [SerializeField] private SkillData _skillData;

    public SkillData SkillData
    {
        get => _skillData;
        set => _skillData = value;
    }

    private ScrScills _scills;

    public Image SkillIcon
    {
        get => _skillIcon;
        set => _skillIcon = value;
    }

    private void Start()
    {
        _scills = FindObjectOfType<ScrScills>();
        _currentCharacter = FindObjectOfType<CurrentCharacter>();
    }

    public void SetParametrs(SkillData skillData)
    {
        _skillData = skillData;
        SkillIcon.sprite = skillData.SkillIcon;
        CurrentLvlText = skillData.SkillLevel;

        if (skillData.SkillLevel > 0)
        {
            foreach (var VARIABLE in _lines)
            {
                VARIABLE.ChangeOnActiv();
            }
        }
        else
        {
            foreach (var VARIABLE in _lines)
            {
                VARIABLE.ChangeOnNonActiv();
            }
        }

        if (skillData.SkillsBlockMe.Count > 0)
        {
            foreach (var VARIABLE in skillData.SkillsBlockMe)
            {
                if (VARIABLE.SkillLevel > 0)
                {
                    SkillIcon.color = Color.gray;
                    break;
                }

                SkillIcon.color = Color.white;
            }
        }
        else
        {
            SkillIcon.color = Color.white;
        }
    }

    public void ShowTooltype()
    {
        _scills.OnTooltyp(_skillData);
    }

    public void LvlReset()
    {
        while (_skillData.SkillLevel > 0)
        {
            _skillData.LvlDown();
//            _currentCharacter.CharacterData.CountSkillPoint++;
        }
    }
}