﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrScills : MonoBehaviour
{
    [SerializeField] private GameObject panelLoading;
    [SerializeField] private Purchase _purchase;
    [SerializeField] private Tooltype _tooltype;
    [SerializeField] private List<CellScill> _cellScill;
    [SerializeField] private Text _countActivPassiv;
    [SerializeField] private Text _countSkillPoint;
    [SerializeField] private Text level3, level6, level12, reLearn;
    [SerializeField] private int buyPrice;
    [SerializeField] private TooltypeAcceptBuy _tooltypeAcceptBuy;
    [SerializeField] private TooltypeNeedPoint tooltypeNeedPoints;
    [SerializeField] private Navigation _navigation;
    private SkillData _currentSkils;
    private int _countActiv, _countPassiv, _countPoint;


    public int CountActiv
    {
        get
        {
            _countActiv = 0;
            for (int i = 0; i < _cellScill.Count; i++)
            {
                if (_currentCharacter.CharacterData.CharSkills[i].isInvestigated)
                {
                    if (_currentCharacter.CharacterData.CharSkills[i].skillType == SkillData.SkillType.Active)
                    {
                        _countActiv++;
                    }
                }
            }

            return _countActiv;
        }
        set => _countActiv = value;
    }

    public int CountPassiv
    {
        get
        {
            _countPassiv = 0;
            for (int i = 0; i < _cellScill.Count; i++)
            {
                if (_currentCharacter.CharacterData.CharSkills[i].isInvestigated)
                {
                    if (_currentCharacter.CharacterData.CharSkills[i].skillType == SkillData.SkillType.Passive)
                    {
                        _countPassiv++;
                    }
                }
            }

            return _countPassiv;
        }
        set => _countPassiv = value;
    }

    public int CountPoint
    {
        get => _countPoint;
        set => _countPoint = value;
    }

    private CurrentCharacter _currentCharacter;

    private void Awake()
    {
        _currentCharacter = FindObjectOfType<CurrentCharacter>();
    }

    public Tooltype Tooltype
    {
        get => _tooltype;
        set => _tooltype = value;
    }

    private void OnEnable()
    {
        level3.text = "3 " + Config.Constant.LEVEL[Config.CURRENT_LANGUAGE];
        level6.text = "6 " + Config.Constant.LEVEL[Config.CURRENT_LANGUAGE];
        level12.text = "12 " + Config.Constant.LEVEL[Config.CURRENT_LANGUAGE];
        reLearn.text = Config.Constant.RELEARN_SKILLS_FOR[Config.CURRENT_LANGUAGE] + buyPrice;
        _countActiv = 0;
        _countPassiv = 0;
        for (int i = 0; i < _cellScill.Count; i++)
        {
            _cellScill[i].SetParametrs(_currentCharacter.CharacterData.CharSkills[i]);

            if (_currentCharacter.CharacterData.CharSkills[i].isInvestigated)
            {
                if (_currentCharacter.CharacterData.CharSkills[i].skillType == SkillData.SkillType.Active)
                {
                    _countActiv++;
                }
                else
                {
                    _countPassiv++;
                }
            }
        }

        UpdateTextArea();
    }

    public void OnTooltyp(SkillData _skillData)
    {
        Tooltype.SetParametrs(_skillData);
        Tooltype.gameObject.SetActive(true);
        _currentSkils = _skillData;
    }

    public void LvlReset(int cost)
    {
        if (_purchase.PurchaseForCristal(cost))
        {
            foreach (var VARIABLE in _cellScill)
            {
                VARIABLE.LvlReset();
            }

            OnEnable();
            UpdateCharacterSkills();
        }
        else
        {
            _tooltypeAcceptBuy.gameObject.SetActive(true);
            _tooltypeAcceptBuy.SetText(false);
        }
    }

    public void SkillLvlUp()
    {
        if (_currentSkils.LvlUp())
        {
            OnEnable();
            UpdateCharacterSkills();
        }

        else
        {
            tooltypeNeedPoints.gameObject.SetActive(true);
            if (_currentCharacter.CharacterData.CountSkillPoint > 0)
            {
                tooltypeNeedPoints.SetText(Config.Constant.NEED_LVL[Config.CURRENT_LANGUAGE]);
            
            }
            else
                tooltypeNeedPoints.SetText(Config.Constant.NEED_POINTS[Config.CURRENT_LANGUAGE]);
        }
    }

    private void UpdateTextArea()
    {
        _countActivPassiv.text = Config.Constant.LEARNED[Config.CURRENT_LANGUAGE] + "\n" +
                                 Config.Constant.ACTIV[Config.CURRENT_LANGUAGE] + _countActiv + "\n" +
                                 Config.Constant.PASSIV[Config.CURRENT_LANGUAGE] + _countPassiv;
        _countSkillPoint.text = Config.Constant.AVAILABLE_UPDATE[Config.CURRENT_LANGUAGE] +
                                _currentCharacter.CharacterData.CountSkillPoint;
        
    }

    private void UpdateCharacterSkills()
    {
        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        for (int i = 0; i < _currentCharacter.CharacterData.CharSkills.Length; i++)
        {
            charId.CharIdLvls.Find(x => x.current).skills.SkillsLvlUp[i].skillLvl =
                _currentCharacter.CharacterData.CharSkills[i].SkillLevel;
        }

        UserData.Hiro = JsonUtility.ToJson(charId);

        StartCoroutine(SendChar(UserData.Hiro));
        _navigation.UpdateImage();
    }

    public IEnumerator SendChar(string json)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }
}