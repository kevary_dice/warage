﻿using UnityEngine;
using UnityEngine.UI;

public class ScrSetting : MonoBehaviour
{
    [SerializeField] private Text id,
        version,
        gameSettings,
        languageText,
        sounds,
        skillInFight,
        updateGraphics,
        userAds,
        confidetial,
        btnAccepText;

    [SerializeField] private ToggleCustom sound, skill, graphics;
    [SerializeField] private Dropdown language;

    private void OnEnable()
    {
        id.text = "id: " + UserData.Index;
        version.text = "version: " + Application.version;
        gameSettings.text = Config.Constant.GAME_SETTINGS[Config.CURRENT_LANGUAGE];
        languageText.text = Config.Constant.LANGUAGE[Config.CURRENT_LANGUAGE];
        sounds.text = Config.Constant.SOUNDS[Config.CURRENT_LANGUAGE];
//      enabled.text = Config.Constant.FORCE[Config.CURRENT_LANGUAGE];
        skillInFight.text = Config.Constant.SKILL_IN_FIGHT[Config.CURRENT_LANGUAGE];
        updateGraphics.text = Config.Constant.UPDATE_GRAPHICS[Config.CURRENT_LANGUAGE];
        userAds.text = Config.Constant.USER_POLICY[Config.CURRENT_LANGUAGE];
        confidetial.text = Config.Constant.PRIVACY_POLICY[Config.CURRENT_LANGUAGE];
        btnAccepText.text = Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE];

        sound.isOffTxt = Config.Constant.DISABLED[Config.CURRENT_LANGUAGE];
        sound.isOnTxt = Config.Constant.ENABLED[Config.CURRENT_LANGUAGE];
        sound.ChangeTxt();
        skill.isOffTxt = Config.Constant.LEFT[Config.CURRENT_LANGUAGE];
        skill.isOnTxt = Config.Constant.RIGHT[Config.CURRENT_LANGUAGE];
        skill.ChangeTxt();
        graphics.isOffTxt = Config.Constant.DISABLED[Config.CURRENT_LANGUAGE];
        graphics.isOnTxt = Config.Constant.ENABLED[Config.CURRENT_LANGUAGE];
        graphics.ChangeTxt();
    }

    public void OnChangeDropdown()
    {
        Config.CURRENT_LANGUAGE = language.value;
        OnEnable();
    }
}