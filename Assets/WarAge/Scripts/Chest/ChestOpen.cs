﻿using UnityEngine;

public class ChestOpen : MonoBehaviour
{
    [SerializeField] private float animDuration = 0.5f;
    private void OnEnable()
    {

        Invoke("AfterOpen",animDuration);
    }

    public void AfterOpen()
    {
        Destroy(gameObject);
    }
}
