﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class ChestPanel : MonoBehaviour
{
    [SerializeField] private GameObject panelChest;
    [SerializeField] private TooltypeInventory panelItem;
    [SerializeField] private MainMenuManager _mainMenuManager;
    [SerializeField] private int _id;
    [SerializeField] private int _timerOpenChest;
    [SerializeField] private GameObject parentForChest;
    [SerializeField] private GameObject chestPrefabe;
    [SerializeField] private List<Rarity> canDrop;
    [SerializeField] private Purchase _purchase;
    [SerializeField] private TooltypeAcceptBuy _tooltypeAcceptBuy;
    [SerializeField] private Navigation chestBtn;
    public int TimerOpenChest
    {
        get => _timerOpenChest;
        set
        {
            _timerOpenChest = value;
            TimeConverter timeConverter = new TimeConverter(_timerOpenChest);
            _timerText.text =Config.Constant.GRATUIT_AFTER[Config.CURRENT_LANGUAGE] + timeConverter;
        }
    }

    [SerializeField] private int _cost;
    [SerializeField] private Config Config;
    [SerializeField] private Text _timerText;
    [SerializeField] private Button _button;
    [SerializeField] private GameObject crystalIcon;
     private float animDuration = 3f;

    private void Start()
    {
        _button.onClick.AddListener(PressButtonChest);
        StartCoroutine(QueryChest("queryTimer"));
    }

    private void OnEnable()
    {
        _button.GetComponentInChildren<Text>().text =Config.Constant.OPEN[Config.CURRENT_LANGUAGE] +"- "+_cost;
        StartCoroutine(QueryChest("queryTimer"));
        StartCoroutine(Timer());
    }

    public void PressButtonChest()
    {
        if (TimerOpenChest <= 0)
        {
            StartCoroutine(OpenFreeChest());
            
        }
        else
        {
            BuyChest();
        }
    }

    public void BuyChest()
    {
        if (_purchase.PurchaseReturn(CharacterData.Currency.Crystal,_cost))
        {
            OpenChest();
        }
        else
        {
            _tooltypeAcceptBuy.gameObject.SetActive(true);
            _tooltypeAcceptBuy.SetText(false);
            Debug.LogError("А где деньги?!");
        }
    }

    IEnumerator OpenFreeChest()
    {
        //TODO Анимация открытия
        yield return StartCoroutine(QueryChest("queryTimer"));
        if (_timerOpenChest <= 0)
        {
            OpenChest();
            StartCoroutine(QueryChest("sendOpen"));
            chestBtn.UpdateImage();
            StartCoroutine(Timer());
        }
        else
        {
            StartCoroutine(Timer());
        }
    }

    void OpenChest()
    {
        //todo открытие сундука
       Item item=  ChoseItem();
//        Instantiate(chestPrefabe, parentForChest.transform);
        panelChest.SetActive(true);
        

        
//        _button.GetComponentInChildren<Text>().text =Config.Constant.BUY_FOR[Config.CURRENT_LANGUAGE] + _cost+" " + Config.Constant.CRYSTALS_SMALL_KEYS[Config.CURRENT_LANGUAGE];
        
        _button.GetComponentInChildren<Text>().text =Config.Constant.OPEN[Config.CURRENT_LANGUAGE] +"- "+_cost;
        
        crystalIcon.SetActive(true);
        _button.GetComponent<Image>().color = Color.white;
        StartCoroutine(ActivatePanelItem(item));

    }

    IEnumerator ActivatePanelItem(Item item)
    {
        chestPrefabe.SetActive(true);
//        yield return new WaitForSeconds(1f);
        chestPrefabe.GetComponent<Animator>().Play("open");
        
        yield return new WaitForSeconds(animDuration); 
        
        chestPrefabe.SetActive(false);
        
        panelItem.gameObject.SetActive(true);
        panelChest.SetActive(false);
        panelItem.SetParametrs(item,true);
    }


    IEnumerator QueryChest(string action)
    {
        WWWForm form = new WWWForm();
        form.AddField("userId", UserData.Id);
        form.AddField("chestId", _id);
        form.AddField("action", action);
        WWW www = new WWW(Config.url, form);
        yield return www;

        Debug.LogError(www.text);
        UserData.ChestTimeToJson(www.text);
        TimerOpenChest = UserData.ChestTime.ChestTimes[_id];
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1f);

        if (TimerOpenChest > 0)
        {
            TimerOpenChest--;
            
            crystalIcon.SetActive(true);
            _button.GetComponent<Image>().color = Color.white;
            StartCoroutine(Timer());
        }
        else
        {
            _button.GetComponentInChildren<Text>().text =Config.Constant.OPEN[Config.CURRENT_LANGUAGE] ;
            crystalIcon.SetActive(false);
            _button.GetComponent<Image>().color = Color.green;
        }
    }

    public Item ChoseItem()
    {
        List<Item> items = Config.AllItem.FindAll(x =>
            x.itemTyepe == Item.ItemType.Equip ||
            x.itemTyepe == Item.ItemType.Weapon);
        List<Item> nededItem = new List<Item>();

        foreach (var VARIABLE in items)
        {
            try
            {
                if (canDrop.Exists(x => x == (VARIABLE as ItemEquip).rarity))
                {
                    nededItem.Add(VARIABLE);
                }
            }
            catch (Exception e)
            {
                if (canDrop.Exists(x => x == (VARIABLE as ItemWeapon).rarity))
                {
                    nededItem.Add(VARIABLE);
                }
            }
        }
        Random rand=new Random();
        Item currentItem = nededItem[rand.Next(nededItem.Count)];
        Inventory inventory = new Inventory()
        {
            itemId = Config.AllItem.IndexOf(
               Config.AllItem.Find(x => x.ItemName == currentItem.ItemName)),
            ItemCount = 1
        };
        UserData.AddInventory(inventory);
        StartCoroutine(SetInventory(JsonUtility.ToJson(UserData.Inventory)));
        return currentItem;
    }
    IEnumerator SetInventory(string json)
    {
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        Debug.LogError(json);
        form.AddField("inventory", json);
        form.AddField("action", "setInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);

    }
    void StopTimer()
    {
        StopAllCoroutines();
    }
}

[Serializable]
public class ChestTime
{
    public List<int> ChestTimes;

    public ChestTime()
    {
        ChestTimes = new List<int>();
    }
}

public class TimeConverter
{
    public int Day;
    public int Hour;
    public int Minut;
    public int Seconds;

    public TimeConverter(int _time)
    {
        Day = _time / 24 / 3600;
        _time %= 24 * 3600;
        Hour = _time / 3600;
        _time %= 3600;
        Minut = _time / 60;
        _time %= 60;
        Seconds = _time;
    }

    public void Converter(int _time)
    {
        Day = _time / 24 / 3600;
        _time %= 24 * 3600;
        Hour = _time / 3600;
        _time %= 3600;
        Minut = _time / 60;
        _time %= 60;
        Seconds = _time;
    }

    public override string ToString()
    {
        if (Day != 0)
        {
            return Day + ":" + Hour + ":" + Minut + ":" + Seconds;
        }

        if (Hour != 0)
        {
            return Hour + ":" + Minut + ":" + Seconds;
        }

        if (Minut != 0)
        {
            return Minut + ":" + Seconds;
        }

        return Seconds.ToString();
    }
}