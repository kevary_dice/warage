﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TooltypeAcceptBuy : MonoBehaviour,IPointerClickHandler
{
    [SerializeField] private Text textInfo;
  
    public void OnPointerClick(PointerEventData eventData)
    {
        OffMe();
    }

    public void SetText(bool text,int count=0)
    {
       
        if (text)
        {
            textInfo.text =Config.Constant.WOW_YOU_BUY[Config.CURRENT_LANGUAGE] +count;
        }
        else
        {
            textInfo.text =Config.Constant.NEED_FUNDS[Config.CURRENT_LANGUAGE];
        }
    }

    public void OffMe()
    {
        gameObject.SetActive(false);
    }

   
}
