﻿using UnityEngine;
using UnityEngine.UI;

public class ScrnChess : MonoBehaviour
{
    [SerializeField]
    private Text bigSale, superSale, chests, comonChest, daemonChest, titleComonChest, titleDaemonChest,crystals,
        heapCrystal,pileCrystal,sacCrystal,gold,goldSmalKeys1,goldSmalKeys2,goldSmalKeys3,battlePass,battlePassSmallKeys1,battlePassSmallKeys2,battlePassSmallKeys3;
    private void OnEnable()
    {
        bigSale.text = Config.Constant.BIG_SALE[Config.CURRENT_LANGUAGE];
        superSale.text = Config.Constant.SUPER_SALE[Config.CURRENT_LANGUAGE];
        chests.text = Config.Constant.CHESTS[Config.CURRENT_LANGUAGE];
        comonChest.text = Config.Constant.COMMON_CHEST[Config.CURRENT_LANGUAGE];
        daemonChest.text = Config.Constant.DAEMON_CHEST[Config.CURRENT_LANGUAGE];
        titleComonChest.text = Config.Constant.TITLE_COMMON_CHEST[Config.CURRENT_LANGUAGE];
        titleDaemonChest.text = Config.Constant.TITLE_DAEMON_CHEST[Config.CURRENT_LANGUAGE];
        crystals.text = Config.Constant.CRYSTALS[Config.CURRENT_LANGUAGE];
        heapCrystal.text = Config.Constant.HEAP_CRYSTAL[Config.CURRENT_LANGUAGE];
        pileCrystal.text = Config.Constant.PILE_CRYSTAL[Config.CURRENT_LANGUAGE];
        sacCrystal.text = Config.Constant.SAC_CRYSTAL[Config.CURRENT_LANGUAGE];
        gold.text = Config.Constant.GOLD[Config.CURRENT_LANGUAGE];
        goldSmalKeys1.text = "+2900 "+Config.Constant.GOLD_SMALL_KEYS[Config.CURRENT_LANGUAGE];
        goldSmalKeys2.text ="+9000 "+ Config.Constant.GOLD_SMALL_KEYS[Config.CURRENT_LANGUAGE];
        goldSmalKeys3.text ="+30000 "+ Config.Constant.GOLD_SMALL_KEYS[Config.CURRENT_LANGUAGE];
        battlePass.text = Config.Constant.BATTLE_PASS[Config.CURRENT_LANGUAGE];
        battlePassSmallKeys1.text = "+30 "+ Config.Constant.BATTLE_PASS_CAMEL_CASE[Config.CURRENT_LANGUAGE];
        battlePassSmallKeys2.text = "+100 "+ Config.Constant.BATTLE_PASS_CAMEL_CASE[Config.CURRENT_LANGUAGE];
        battlePassSmallKeys3.text = "+300 "+ Config.Constant.BATTLE_PASS_CAMEL_CASE[Config.CURRENT_LANGUAGE];
        
    }
}
