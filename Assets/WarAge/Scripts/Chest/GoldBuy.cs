﻿using UnityEngine;
using UnityEngine.UI;

public class GoldBuy : MonoBehaviour
{
    [SerializeField] private int cost;
    [SerializeField] private int addGold;
    [SerializeField] private CharacterData.Currency _currencyAdd = CharacterData.Currency.Gold;
    [SerializeField] private CharacterData.Currency _currencyCost = CharacterData.Currency.Crystal;
    [SerializeField] private Purchase _purchase;
    [SerializeField] private TooltypeAcceptBuy _tooltypeAcceptBuy;

    private void Awake()
    {
       
        _purchase = FindObjectOfType<Purchase>();
        GetComponent<Button>().onClick.AddListener(ByeGold);
    }

    public void ByeGold()
    {
        _tooltypeAcceptBuy.gameObject.SetActive(true);
        _tooltypeAcceptBuy.SetText(_purchase.CheckPurchase(_currencyCost, cost),addGold);
        if (_purchase.PurchaseReturn(_currencyCost, cost))
        {
            _purchase.PurchaseReturn(_currencyAdd, -addGold);
        }
    }
}