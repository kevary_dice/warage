﻿
using System;
using System.Collections.Generic;
[Serializable]
public class Constant
{
    public static string pathToJson = "/StreamingAssets/Language.json";
    public static string Json = "Language.json";
    public  List<string> PASSWORD = new List<string>(){};
    public  List<string> REMEMBER_ME = new List<string>(){};
    public  List<string> FORGET_PASSWORD = new List<string>(){};
    public  List<string> LOGIN = new List<string>(){};
    public  List<string> OR = new List<string>(){};
    public  List<string> AUTHORIZATION = new List<string>(){};
    
    public  List<string> FORCE = new List<string>(){};
    public  List<string> AGILITY = new List<string>(){};
    public  List<string> INTELLIGENCE = new List<string>(){};
    public  List<string> ARMOR_SMALL_KEYS = new List<string>(){};
    public  List<string> LVL = new List<string>(){};
    public  List<string> DAMAGE = new List<string>(){};
    public  List<string> REUSE = new List<string>(){};
    public  List<string> STUN_ENEMY_FOR = new List<string>(){};
    public  List<string> NEED = new List<string>(){};
    public  List<string> BASE_PARAM = new List<string>(){};
    public  List<string> INFORMATION = new List<string>(){};
    public  List<string> SKILLS = new List<string>(){};
    public  List<string> MELEE = new List<string>(){};
    public  List<string> RANGE = new List<string>(){};
    public  List<string> MAGIC = new List<string>(){};
    public  List<string> BUY_FOR = new List<string>(){};
    public  List<string> WEAPON_TITLE = new List<string>(){};
    public List<string> HELMET = new List<string>() { };
    public  List<string> NEED_LVL = new List<string>(){};
    
    public  List<string> PUT_ON = new List<string>(){};
    public  List<string> PUT_OFF = new List<string>(){};
    public  List<string> CLOSE = new List<string>(){};
 
    public  List<string> SUCCESS_BUY = new List<string>(){};
        
    public  List<string> SELL = new List<string>(){};
    public  List<string> ACCEPT = new List<string>(){};
    public  List<string> ACCEPT_BUY = new List<string>(){};
    public  List<string> NEED_FUNDS = new List<string>(){};
    public  List<string> NEED_POINTS = new List<string>(){};
    public  List<string> CHOOSE_CHARACTER = new List<string>(){};
    
    public  List<string> YOU_CAN_UPDATE_CHARACTER = new List<string>(){};
    public  List<string> DISK_UPDATE = new List<string>(){};
    public  List<string> AVAILABLE = new List<string>(){};
    public  List<string> UPDATES = new List<string>(){};
    
    public  List<string> P_ATTACK = new List<string>(){};
    public  List<string> MAX_HP = new List<string>(){};
    public  List<string> SPEED = new List<string>(){};
    public  List<string> ATTACK_SPEED = new List<string>(){};
    public  List<string> M_ATTACK = new List<string>(){};
    public  List<string> MAX_MP = new List<string>(){};
    
    public  List<string> UPDATE_CHARACTER = new List<string>(){};
    public  List<string> CHANGE_CHARACTER = new List<string>(){};
    
    public  List<string> NEED_ITEM = new List<string>(){};
    
    public  List<string> LEARNED = new List<string>(){};
    public  List<string> LEARN = new List<string>(){};
    public  List<string> ACTIV = new List<string>(){};
    public  List<string> PASSIV = new List<string>(){};
    public  List<string> AVAILABLE_UPDATE = new List<string>(){};
    public  List<string> LEVEL = new List<string>(){};
    public  List<string> RELEARN_SKILLS_FOR = new List<string>(){};
    
    public  List<string> SHOP_BLACKSMITH = new List<string>(){};
    
    public  List<string> SEND = new List<string>(){};
    public  List<string> POTION = new List<string>(){};
    public  List<string> WEAPON = new List<string>(){};
    public  List<string> ARMOR_AND_SHIELD = new List<string>(){};
    public  List<string> NECLEACE = new List<string>(){};
    public  List<string> RING = new List<string>(){};
    
    public  List<string> MELEE_WEAPON = new List<string>(){};
    public  List<string> MAGIC_WEAPON = new List<string>(){};
    public  List<string> BOW_WEAPON = new List<string>(){};
    
    public  List<string> ARMOR = new List<string>(){};
    public  List<string> GLOVES = new List<string>(){};
    public  List<string> BOOTS = new List<string>(){};
    public  List<string> SHIELD = new List<string>(){};
    public  List<string> WOW_YOU_BUY = new List<string>(){};
    
    public  List<string> SET_NAME = new List<string>(){};
    public  List<string> USER_NAME = new List<string>(){};
    public  List<string> DISC_ON_SET_NAME = new List<string>(){};
    
    public  List<string> CHESTS = new List<string>(){};
    public  List<string> INVENTORY = new List<string>(){};
    public  List<string> FIGHT = new List<string>(){};
    public  List<string> SHOP = new List<string>(){};
    
    public  List<string> RESUME = new List<string>(){};
    public  List<string> SETTINGS = new List<string>(){};
    public  List<string> FEEDBACK = new List<string>(){};
    public  List<string> EXIT = new List<string>(){};
    public  List<string> BACK = new List<string>(){};
    
    public  List<string> TRAIN_CAMP = new List<string>(){};
    public  List<string> GAME = new List<string>(){};
    
    public  List<string> ACTIVE_SKILLS = new List<string>(){};
    public  List<string> PASSIVE_SKILLS = new List<string>(){};
    
    public  List<string> COMMON_CHEST = new List<string>(){};
    public  List<string> TITLE_COMMON_CHEST = new List<string>(){};
    public  List<string> DAEMON_CHEST = new List<string>(){};
    public  List<string> TITLE_DAEMON_CHEST = new List<string>(){};
    public  List<string> GRATUIT_AFTER = new List<string>(){};
    public  List<string> CRYSTALS_SMALL_KEYS = new List<string>(){};
    public  List<string> OPEN = new List<string>(){};
    public  List<string> CRYSTALS = new List<string>(){};
    public  List<string> HEAP_CRYSTAL = new List<string>(){};
    public  List<string> PILE_CRYSTAL = new List<string>(){};
    
    public  List<string> SAC_CRYSTAL = new List<string>(){};
    public  List<string> GOLD = new List<string>(){};
    public  List<string> GOLD_SMALL_KEYS = new List<string>(){};
    public  List<string> BATTLE_PASS = new List<string>(){};
    public  List<string> BATTLE_PASS_CAMEL_CASE = new List<string>(){};
    
    public  List<string> BIG_SALE = new List<string>(){};
    public  List<string> SUPER_SALE = new List<string>(){};
    
    public  List<string> BACK_TO_AUT = new List<string>(){};
    public  List<string> YOUR_EMAIL = new List<string>(){};
    public  List<string> NEW_PASS = new List<string>(){};
    public  List<string> REPEAT_PASS = new List<string>(){};
    public  List<string> ACCEPT_PRIVACY = new List<string>(){};
    public  List<string> REG = new List<string>(){};
    
    
    public  List<string> RESET_PASS = new List<string>(){};
    public  List<string> RETURN_ACCESS = new List<string>(){};
    public  List<string> ENTER_GOOGLE = new List<string>(){};
    public  List<string> ENTER_FACEBOOK = new List<string>(){};
    
    
    public  List<string> GAME_SETTINGS = new List<string>(){};
    public  List<string> LANGUAGE = new List<string>(){};
    public  List<string> SOUNDS = new List<string>(){};
    public  List<string> ENABLED = new List<string>(){};
    public  List<string> DISABLED = new List<string>(){};
    public  List<string> LEFT = new List<string>(){};
    public  List<string> RIGHT = new List<string>(){};
    public  List<string> SKILL_IN_FIGHT = new List<string>(){};
    public  List<string> UPDATE_GRAPHICS = new List<string>(){};
    public  List<string> USER_POLICY = new List<string>(){};
    public  List<string> PRIVACY_POLICY = new List<string>(){};

}
