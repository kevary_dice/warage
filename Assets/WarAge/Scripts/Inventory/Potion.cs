﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Potion : MonoBehaviour
{
    [SerializeField] private int potionId;
    [SerializeField] private int countPotion;
    [SerializeField] private Text countText;
    [SerializeField] private Inventory _inventory;
    [SerializeField] private MainScreen _mainScreen;
    public int PotionId
    {
        get => potionId;
        set => potionId = value;
    }

 
    public int CountPotion
    {
        get => countPotion;
        set
        {  
            countPotion = value;
            countText.text = value.ToString();
        }
    }

    

    private void OnEnable()
    {
        try
        {
           
           _inventory = UserData.Inventory.items.Find(x => x.itemId == potionId);
           CountPotion = _inventory.ItemCount;
           
        }
        catch (Exception e)
        {
            CountPotion = 0;
        }
       
    }

    public void ClickBtn()
    {
        if (CountPotion>0)
        {
            UsePotion();
        }
    }

   public void UsePotion()
    {
       
        CurrentCharacter character = FindObjectOfType<CurrentCharacter>();
        
        if (potionId== 0)
        {
            character.CharacterData.RestoreHp(character.CharacterData.maxHp / 2);
            
            
        }
        else
        {
            character.CharacterData.RestoreMp(character.CharacterData.maxMp / 2);
        
        }
        if (_mainScreen!=null)
        {
            _mainScreen.OnEnable();
        }
        PutOn(_inventory);
    }
    
    public void PutOn(Inventory id)
    {
        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        try
        {
            charId.CharIdLvls.Find(x => x.current).itemsId.Add(id.itemId);
        }
        catch (Exception e)
        {
        }

        UserData.RemoveItem(id);
        UserData.Hiro = JsonUtility.ToJson(charId);
        StartCoroutine(SendChar(UserData.Hiro));
        StartCoroutine(SetInventory(JsonUtility.ToJson(UserData.Inventory)));
        OnEnable();
    }
    public IEnumerator SendChar(string json)
    {
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
    }
    
    IEnumerator SetInventory(string json)
    {
        Debug.LogError(json);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("inventory", json);
        form.AddField("action", "setInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
    }
}
