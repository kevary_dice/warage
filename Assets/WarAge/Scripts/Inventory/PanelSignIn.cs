﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PanelSignIn : MonoBehaviour
{
    [SerializeField] private Text password, rememberMe, forgetPassword, login, or, authorisation;

    private void OnEnable()
    {
        LoadJsonLanguage();
    }

    public void UpdateInterface()
    {
        password.text = Config.Constant.PASSWORD[Config.CURRENT_LANGUAGE];
        rememberMe.text = Config.Constant.REMEMBER_ME[Config.CURRENT_LANGUAGE];
        forgetPassword.text = Config.Constant.FORGET_PASSWORD[Config.CURRENT_LANGUAGE];
        login.text = Config.Constant.LOGIN[Config.CURRENT_LANGUAGE];
        or.text = Config.Constant.OR[Config.CURRENT_LANGUAGE];
        authorisation.text = Config.Constant.AUTHORIZATION[Config.CURRENT_LANGUAGE];
    }

    public void LoadJsonLanguage()
    {
        string jsonString;
        StartCoroutine(LoadFile());
//#if UNITY_STANDALONE || UNITY_EDITOR
//        StreamReader stream = new StreamReader(Application.dataPath + Constant.pathToJson);
//        jsonString = stream.ReadToEnd();
//        
//#endif
//        
//#if UNITY_ANDROID
//        StreamReader stream = new StreamReader("jar:file://" + Application.dataPath + "!/assets"+"/Language.json");
//        jsonString = stream.ReadToEnd();
////         jsonString =  File.ReadAllText("jar:file://" + Application.dataPath + "!/assets"+"/Language.json");
//#endif
//        Config.Constant = JsonUtility.FromJson<Constant>(jsonString);
    }

    private IEnumerator LoadFile()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath,Constant.Json);

        if (filePath.Contains("://"))
        {
            WWW www = new WWW(filePath);
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                Config.Constant = JsonUtility.FromJson<Constant>(www.text);
            }
        }
        else
        {
            Config.Constant = JsonUtility.FromJson<Constant>(File.ReadAllText(filePath));
        }

        UpdateInterface();
    }
}