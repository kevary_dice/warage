﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Facebook.MiniJSON;
using UnityEngine;
using UnityEngine.UI;

public class ScrnInvenory : MonoBehaviour
{
    [SerializeField] private Text _hp,
        _mp,
        _force,
        _agility,
        _inteligens,
        _fizDamage,
        _magDamage,
        _armor,
        _speed,
        _attackspeed,
        userNameLvl;


    private List<Inventory> _inventories;
    [SerializeField] private GameObject itemPrefab, panelLoading;

    public GameObject PanelLoading => panelLoading;

    [SerializeField] private Transform parentItemPrefab;
    [SerializeField] private Config config;
    [SerializeField] private Transform _characterContainer;
    [SerializeField] private List<PutOnItem> _putOnItems;

    [SerializeField] private GameObject _contentSizeFitter;

    public Config Config
    {
        get => config;
        set => config = value;
    }

    [SerializeField] private List<InventoryPrefab> inventoryPrefab;
    [SerializeField] private CurrentCharacter _currentCharacter;

    public CurrentCharacter CurrentCharacter => _currentCharacter;

    [SerializeField] private TooltypeInventory _tooltypeInventory;
    [SerializeField] private Text changeChar, needItemText;
    [SerializeField] private Button changeCharBtn;
    [SerializeField] private bool canChange;
    [SerializeField] private GameObject MainScreen, SelectChar;
    [SerializeField] private UpCharacter ScrnUpChar;


    public void CheckChar()
    {
        PointLvlUp _pointLvlUp =
            JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro).CharIdLvls.Find(x => x.current).point;
        if (_pointLvlUp.currentPoint > 0)
        {
            changeChar.text = Config.Constant.UPDATE_CHARACTER[Config.CURRENT_LANGUAGE];
            changeCharBtn.GetComponent<Image>().color = Color.green;
            canChange = false;
        }
        else
        {
            changeChar.text = Config.Constant.CHANGE_CHARACTER[Config.CURRENT_LANGUAGE];
            changeCharBtn.GetComponent<Image>().color = Color.white;
            canChange = true;
        }

//        UpdatePoint();
    }

    private void Awake()
    {
        _currentCharacter = FindObjectOfType<CurrentCharacter>();
    }


    private void OnEnable()
    {
        CheckChar();

        if (_characterContainer.childCount != 0)
        {
            Destroy(_characterContainer.GetChild(0).gameObject);
        }

        UpdateInventory();


        GameObject obj = Instantiate(_currentCharacter.CharacterData.PrefabMenu, _characterContainer);
        obj.transform.localPosition = new Vector3(0, 0, 0);
        obj.transform.localScale = new Vector3(2, 2, 2);
        UpdateText();
    }

    public void UpdateText()
    {
//        if (_currentCharacter==null)
//        {
//            OnEnable();
//            return;
//        }
        _hp.text = _currentCharacter.CharacterData.maxHp.ToString();
        _mp.text = _currentCharacter.CharacterData.maxMp.ToString();
        _force.text = Config.Constant.FORCE[Config.CURRENT_LANGUAGE] + " " +
                      _currentCharacter.CharacterData.force.ToString();
        _agility.text = Config.Constant.AGILITY[Config.CURRENT_LANGUAGE] + " " +
                        _currentCharacter.CharacterData.agility.ToString();
        _inteligens.text = Config.Constant.INTELLIGENCE[Config.CURRENT_LANGUAGE] + " " +
                           _currentCharacter.CharacterData.mind.ToString();
        _fizDamage.text = Config.Constant.P_ATTACK[Config.CURRENT_LANGUAGE] + " " +
                          _currentCharacter.CharacterData.pAttack.ToString();
        _magDamage.text = Config.Constant.M_ATTACK[Config.CURRENT_LANGUAGE] + " " +
                          _currentCharacter.CharacterData.mAttack.ToString();
        _armor.text = Config.Constant.ARMOR_SMALL_KEYS[Config.CURRENT_LANGUAGE] + " " +
                      _currentCharacter.CharacterData.armor.ToString();
        _speed.text = Config.Constant.SPEED[Config.CURRENT_LANGUAGE] + " " +
                      _currentCharacter.CharacterData.moveSpeed.ToString();
        _attackspeed.text = Config.Constant.ATTACK_SPEED[Config.CURRENT_LANGUAGE] + " " +
                            _currentCharacter.CharacterData.spdAttack.ToString();
        userNameLvl.text = UserData.UserName + " " + _currentCharacter.CharacterData.lvl + " lvl";
    }

    private void OnDisable()
    {
        DeleteItem();
    }

    public void SpawnItem()
    {
        int currentPlace = 0;
//        Debug.LogError( config.AllItem[UserData.Inventory.items[0].itemId].isStacable);
        if (UserData.Inventory.items.Count == 0)
        {
            needItemText.gameObject.SetActive(true);
            needItemText.text = Config.Constant.NEED_ITEM[Config.CURRENT_LANGUAGE];
        }
        else
        {
            needItemText.gameObject.SetActive(false);
            for (int i = 0; i < UserData.Inventory.items.Count; i++)
            {
                if (config.AllItem[UserData.Inventory.items[i].itemId].isStacable)
                {
                    InventoryPrefab inv = Instantiate(itemPrefab).GetComponent<InventoryPrefab>();
                    inv.SetIcon(config.AllItem[UserData.Inventory.items[i].itemId]);
                    inv.SetCount(UserData.Inventory.items[i].ItemCount);
                    inv.transform.SetParent(parentItemPrefab, false);
                    inventoryPrefab.Add(inv);

                    currentPlace++;
                }
                else
                {
                    for (int j = currentPlace; j < currentPlace + UserData.Inventory.items[i].ItemCount; j++)
                    {
                        InventoryPrefab inv = Instantiate(itemPrefab).GetComponent<InventoryPrefab>();
                        inv.SetIcon(config.AllItem[UserData.Inventory.items[i].itemId]);
                        inv.transform.SetParent(parentItemPrefab, false);
                        inventoryPrefab.Add(inv);

                    }

                    currentPlace += UserData.Inventory.items[i].ItemCount;
                }


            }
            
            _contentSizeFitter.GetComponent<ContentSizeFitter>().SetLayoutVertical();
        }

        List<int> test = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro).CharIdLvls.Find(x => x.current).itemsId;
        for (int i = 0; i < test.Count; i++)
        {
            foreach (var VARIABLE in _putOnItems)
            {
                if (VARIABLE.ItemType.FindAll(x => x == config.AllItem[test[i]].equipWeaponType).Count > 0)
                {
                    VARIABLE.SetItem(config.AllItem[test[i]]);
                }
            }
        }
        
        parentItemPrefab.GetComponent<ContentSizeFitter>().SetLayoutVertical();
    }

    public void DeleteItem()
    {
//       inventoryPrefab.Clear();
        Debug.LogError("test");
        foreach (var VARIABLE in inventoryPrefab)
        {
            if (VARIABLE != null)
            {
                Destroy(VARIABLE.gameObject);
            }
        }
        inventoryPrefab.RemoveAll(x=>x==null);
    }

    public void BtnClick()
    {
        if (canChange)
        {
            SelectChar.SetActive(true);
            MainScreen.SetActive(false);
        }
        else
        {
            ScrnUpChar.gameObject.SetActive(true);
            ScrnUpChar.SetInventory();
        }
    }

    public void UpdateInventory()
    {
//        _contentSizeFitter.gameObject.SetActive(false);


        DeleteItem();
        StartCoroutine(GetInventory());
        StartCoroutine(SetInventory(JsonUtility.ToJson(UserData.Inventory)));
        SpawnItem();
        parentItemPrefab.GetComponent<ContentSizeFitter>().SetLayoutVertical();
//        LayoutRebuilder.ForceRebuildLayoutImmediate(parentItemPrefab.GetComponent<RectTransform>());
    }

    public void ShowTooltype(Item item, bool isInventory)
    {
        _tooltypeInventory.gameObject.SetActive(true);
        _tooltypeInventory.SetParametrs(item, isInventory);
    }

    public void PutItem(Item item, bool TakeOff)
    {
        panelLoading.SetActive(true);
        if (TakeOff)
            foreach (var VARIABLE in _putOnItems)
            {
                if (VARIABLE.ItemType.FindAll(x => x == item.equipWeaponType).Count > 0)
                {
                    VARIABLE.PutItem(item);
                }
            }
        else
            foreach (var VARIABLE in _putOnItems)
            {
                if (VARIABLE.ItemType.FindAll(x => x == item.equipWeaponType).Count > 0)
                {
                    VARIABLE.PutItem();
                }
            }

//        panelLoading.SetActive(false);
    }


    public void HeapPotion(Inventory id)
    {
        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        try
        {
            charId.CharIdLvls.Find(x => x.current).itemsId.Add(id.itemId);
        }
        catch (Exception e)
        {
        }

        UserData.RemoveItem(id);
        UserData.Hiro = JsonUtility.ToJson(charId);
        StartCoroutine(SendChar(UserData.Hiro));
    }

    public IEnumerator SendChar(string json)
    {
        Debug.LogError("J,yjdbk");
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);

        UpdateInventory();
        panelLoading.SetActive(false);
    }

    public IEnumerator GetInventory()
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
//        Debug.LogError(UserData.Id);
        form.AddField("action", "getInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);

//        Debug.LogError(www.text);
        UserData.InventoryToJson(www.text);
        panelLoading.SetActive(false);
    }

    IEnumerator SetInventory(string json)
    {
        panelLoading.SetActive(true);
        Debug.LogError(json);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("inventory", json);
        form.AddField("action", "setInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }
}