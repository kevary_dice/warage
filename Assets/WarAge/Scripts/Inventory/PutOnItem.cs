﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutOnItem : InventoryPrefab
{
    
    
    [SerializeField] private List<EquipWeaponType> _itemType;

    [SerializeField] private bool isLocked = false;

    [SerializeField] private TooltypeNeedPoint _toolInformation;
    [SerializeField] private PutOnItem blockMe;
    [SerializeField] private bool isEquiped = false;

    public bool IsEquiped
    {
        get => isEquiped;
        set { isEquiped = value; }
    }
    

    public void SetItem(Item _item)
    {
        SetIcon(_item);
        
        IsEquiped = true;
        _scrnInvenory.UpdateText();

        if (!_item.justUse)
        {
            _item.justUse = true;
            SetPoint(_item.justUse);
        }
    }

    public void ReturnItem(int i =1)
    {
        IsEquiped = false;
        if (i==1)
        {
            if (_currentItem.justUse)
            {
                _currentItem.justUse = false;
                SetPoint(_currentItem.justUse);
            }
        }
        
        Return();
        _scrnInvenory.UpdateText();
        
    }

    public void SetPoint(bool value)
    {
         ItemPointUpgrade itemPointUpgrade=new ItemPointUpgrade();
             if (value)
             {
                 
                 try
                 {
                     ItemWeapon weapon =_currentItem as ItemWeapon;
                     
                     itemPointUpgrade.agility = weapon.addAgility;
                     itemPointUpgrade.armor = weapon.addArmor;
                     itemPointUpgrade.force = weapon.addForce;
                     itemPointUpgrade.maxHp = weapon.addMaxHp;
                     itemPointUpgrade.maxMp = weapon.addMaxMp;
                     itemPointUpgrade.inteligence = weapon.addMind;
                     itemPointUpgrade.speed = weapon.addMoveSpeed;
                     itemPointUpgrade.spdAttack = weapon.AddSpdAttack;
                     itemPointUpgrade.pAtack = weapon.addPAttack;
                     itemPointUpgrade.mAtack = weapon.addMAttack;
                 }
                 catch (Exception exception)
                 {
                     ItemEquip weapon = _currentItem as ItemEquip;
                     
                     itemPointUpgrade.agility = weapon.addAgility;
                     itemPointUpgrade.armor = weapon.addArmor;
                     itemPointUpgrade.force = weapon.addForce;
                     itemPointUpgrade.maxHp = weapon.addMaxHp;
                     itemPointUpgrade.maxMp = weapon.addMaxMp;
                     itemPointUpgrade.inteligence = weapon.addMind;
                     itemPointUpgrade.speed = weapon.addMoveSpeed;
                     itemPointUpgrade.spdAttack = weapon.AddSpdAttack;
                     itemPointUpgrade.pAtack = weapon.addPAttack;
                     itemPointUpgrade.mAtack = weapon.addMAttack;
                 }
                 
                
             }
             else
             {
                
                 try
                 {
                     ItemWeapon weapon =_currentItem as ItemWeapon;
                     itemPointUpgrade.agility = -weapon.addAgility;
                     itemPointUpgrade.armor =- weapon.addArmor;
                     itemPointUpgrade.force = -weapon.addForce;
                     itemPointUpgrade.maxHp =- weapon.addMaxHp;
                     itemPointUpgrade.maxMp = -weapon.addMaxMp;
                     itemPointUpgrade.inteligence = -weapon.addMind;
                     itemPointUpgrade.speed =- weapon.addMoveSpeed;
                     itemPointUpgrade.spdAttack = -weapon.AddSpdAttack;
                     itemPointUpgrade.pAtack = -weapon.addPAttack;
                     itemPointUpgrade.mAtack =- weapon.addMAttack;
                     Debug.LogError("Снимаю");
                 }
                 catch (Exception exception)
                 {
                     ItemEquip weapon = _currentItem as ItemEquip;
                     
                     itemPointUpgrade.agility = -weapon.addAgility;
                     itemPointUpgrade.armor =- weapon.addArmor;
                     itemPointUpgrade.force = -weapon.addForce;
                     itemPointUpgrade.maxHp =- weapon.addMaxHp;
                     itemPointUpgrade.maxMp = -weapon.addMaxMp;
                     itemPointUpgrade.inteligence = -weapon.addMind;
                     itemPointUpgrade.speed =- weapon.addMoveSpeed;
                     itemPointUpgrade.spdAttack = -weapon.AddSpdAttack;
                     itemPointUpgrade.pAtack = -weapon.addPAttack;
                     itemPointUpgrade.mAtack =- weapon.addMAttack;
                     Debug.LogError("Снимаю");
                 }
                 
                
             }
             _scrnInvenory.CurrentCharacter.CharacterData.UpgradeItem(itemPointUpgrade); 
             
        
    }
    public List<EquipWeaponType> ItemType => _itemType;

    public void PutItem(Item _item = null){
//    {Debug.LogError((isEquiped).ToString()+(!isLocked).ToString()+(_currentItem!=null).ToString()+(_item != null&&_item.NeededLevel<=FindObjectOfType<CurrentCharacter>().CharacterData.lvl).ToString());
        if (isEquiped&&!isLocked&&_currentItem!=null&&(_item == null||_item.NeededLevel<=FindObjectOfType<CurrentCharacter>().CharacterData.lvl))
        {
//            Debug.LogError(_currentItem.itemNames[Config.CURRENT_LANGUAGE]);
            Inventory inventory = new Inventory()
            {
                itemId = _scrnInvenory.Config.AllItem.IndexOf(
                    _scrnInvenory
                        .Config
                        .AllItem
                        .Find(x =>
                        x.itemNames[Config.CURRENT_LANGUAGE] == _currentItem.itemNames[Config.CURRENT_LANGUAGE])),
                ItemCount = 1
            };
            TakeOff(inventory);
        }

        if (_item != null&&_item.NeededLevel<=FindObjectOfType<CurrentCharacter>().CharacterData.lvl)
        {
            try
            {
                ItemWeapon weapon =_item as ItemWeapon;
                
                    Set(weapon);
                    if (weapon.isTwoHeand)
                    {
                        blockMe.PutItem();
                    }
                
                
               
            }
            catch (Exception e)
            {
               
                Set(_item);
                if (_currentItem.equipWeaponType==EquipWeaponType.Shield)
                {
                    ItemWeapon weapon =blockMe._currentItem as ItemWeapon;
                    try
                    {
                        if (weapon.isTwoHeand)
                        {
                            blockMe.PutItem();
                        }
                    }
                    catch (Exception exception)
                    {
                    }
                    
                }
                
            }
            
            
        }
        else if (_item!=null)
        {
            _toolInformation.gameObject.SetActive(true);
            _toolInformation.SetText(Config.Constant.NEED_LVL[Config.CURRENT_LANGUAGE]+" "+_item.NeededLevel);
            
        }

        StartCoroutine(Test(JsonUtility.ToJson(UserData.Inventory)));
    }

    public void Set(Item item)
    {
        SetItem(item);
        Inventory inventory = new Inventory()
        {
            itemId = _scrnInvenory.Config.AllItem.IndexOf(
                _scrnInvenory.Config.AllItem.Find(x => x.itemNames[Config.CURRENT_LANGUAGE] == item.itemNames[Config.CURRENT_LANGUAGE])),
            ItemCount = 1
        };
        PutOn(inventory);
    }
    public void TakeOff(Inventory id)
    {
        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        try
        {
            charId.CharIdLvls.Find(x => x.current).itemsId.Remove(id.itemId);
        }
        catch (Exception e)
        {
        }

        UserData.Hiro = JsonUtility.ToJson(charId);
        UserData.AddInventory(id);
        ReturnItem();
        StartCoroutine(SendChar(UserData.Hiro));
    }

    public void PutOn(Inventory id)
    {
        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        try
        {
            charId.CharIdLvls.Find(x => x.current).itemsId.Add(id.itemId);
        }
        catch (Exception e)
        {
        }

        UserData.RemoveItem(id);
        UserData.Hiro = JsonUtility.ToJson(charId);
        StartCoroutine(SendChar(UserData.Hiro));
    }

    IEnumerator Test(string json)
    {
        yield return StartCoroutine(SetInventory(json));
        _scrnInvenory.UpdateInventory();
    }

    IEnumerator SetInventory(string json)
    {
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
//        Debug.LogError(json);
        form.AddField("inventory", json);
        form.AddField("action", "setInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;
//        _shopPanel.UpdateInventory();
        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
//        OffToolType();
    }
    public IEnumerator SendChar(string json)
    {
        Debug.LogError("check");
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
    }
    public void ShowTooltype()
    {
        if (isEquiped)
        {
            _scrnInvenory.ShowTooltype(_currentItem, false);
        }
    }
    
}