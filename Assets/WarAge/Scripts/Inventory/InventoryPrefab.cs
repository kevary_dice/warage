﻿using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPrefab : MonoBehaviour
{
    [SerializeField] private bool isInventory = true;
    [SerializeField] private Sprite startColor;
    [SerializeField] private RawImage rarityBg;
    [SerializeField] private Image rarityBgImage;
    [SerializeField] private Color standartColor;
    [SerializeField] private Image itemIcon;
    [SerializeField, ReadOnly(false)] private bool isFull;
    [SerializeField] protected ScrnInvenory _scrnInvenory;
    [SerializeField] protected Item _currentItem;
    [SerializeField] private Text countStack;
    [SerializeField] private Config config;


    private void Start()
    {
        _scrnInvenory = FindObjectOfType<ScrnInvenory>();
//        config = FindObjectOfType<Config>();
//        standartColor = rarityBg.color;
    }

    private void OnDisable()
    {
        Return();
    }

    private void OnEnable()
    {
        if (_currentItem==null)
        {
            if (rarityBg == null)
            {
                rarityBgImage.color = standartColor;
            }
            else
                rarityBg.color = standartColor;
        }
        
    }

    public void SetIcon(Item item)
    {
//        Debug.LogError("enter");
        isFull = true;
        _currentItem = item;
        itemIcon.sprite = item.ItemIcon;
        try
        {
           
            ItemEquip itemEquip = item as ItemEquip;
//            Debug.LogError((int)itemEquip.rarity);
            if (rarityBgImage != null)
            {
                rarityBgImage.color = config.rarityColor[(int) itemEquip.rarity];
            }
            else
                rarityBg.color = config.rarityColor[(int) itemEquip.rarity];
        }
        catch (Exception e)
        {
            try
            {
                ItemWeapon itemEquip = item as ItemWeapon;
                if (rarityBgImage != null)
                {
                    Debug.LogError("это  "+gameObject.name);
                    rarityBgImage.color = config.rarityColor[(int) itemEquip.rarity];
                }
                else
                    rarityBg.color = config.rarityColor[(int) itemEquip.rarity];
            }
            catch (Exception exception)
            {
                if (rarityBgImage != null)
                {
                    rarityBgImage.color = standartColor;
                }
                else
                    rarityBg.color = standartColor;
            }
        }
    }

    public void ShowTooltype()
    {
        if (isFull)
            _scrnInvenory.ShowTooltype(_currentItem, isInventory);
    }

    public void Return()
    {
        itemIcon.sprite = startColor;
        _currentItem = null;
        isFull = false;
        if (_currentItem==null)
        {
            if (rarityBg == null)
            {
                rarityBgImage.color = standartColor;
            }
            else
                rarityBg.color = standartColor;
        }
    }

    public void SetCount(int i)
    {
        if (i == 1)
        {
            countStack.text = "";
        }
        else countStack.text = "x" + i;
    }
}