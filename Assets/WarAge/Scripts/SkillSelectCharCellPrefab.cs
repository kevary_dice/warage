﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSelectCharCellPrefab : MonoBehaviour
{
    public int index;
   
    public Image icon; 
    private Tooltype _tooltype;
    [SerializeField] private SkillData _skillData;
    [SerializeField] private SelectCharacter _parentTooltype;

    private void Start()
    {
        _parentTooltype = FindObjectOfType<SelectCharacter>();
//        _tooltype = FindObjectOfType<Tooltype>();
    }

    public SkillData SkillData
    {
        get => _skillData;
        set => _skillData = value;
    }

    public void OnTooltyp()
    {
        
//        _tooltype = Instantiate(_tooltype,FindObjectOfType<SelectCharacter>().gameObject.transform);
        _parentTooltype.Tooltype.SetParametrs(_skillData);
        _parentTooltype.Tooltype.gameObject.SetActive(true);
    }
}
