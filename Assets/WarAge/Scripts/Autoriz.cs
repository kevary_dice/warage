﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Autoriz : MonoBehaviour
{
    public Config Config;
    public GameObject loading;
    [SerializeField] private string id, name, email;

    public void test()
    {
        StartCoroutine(LoginCar(id, name, email));
    }
    public void Inicializ(string id, string userName, string email)
    {
        StartCoroutine(LoginCar(id, userName, email));
    }

    public IEnumerator LoginCar(string userId, string userName, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("userId", userId);
        form.AddField("userName", userName);
        form.AddField("email", email);
        form.AddField("action", "auth");
        WWW www = new WWW(Config.url, form);
        yield return www;

        Data dd = JsonUtility.FromJson<Data>(www.text);
        if (dd.id != "")
        {
            UserData.Id = dd.id;
            UserData.Email = dd.email;
            UserData.UserName = dd.userName;
            UserData.DuelJson = dd.duel;
            UserData.Gold = dd.gold;
            UserData.Cristal = dd.cristal;


            UserData.Hiro = dd.hiro;
            
            if (UserData.Hiro == null)
            {
                UserData.Hiro = "";
            }

            if (dd.inventory!=null)
            {
                UserData.InventoryToJson(dd.inventory);
            }
            
            UserData.OtherData = dd.otherData;
            UserData.Index = dd.index;
            UserData.ChestTimeToJson(dd.chestTime);
            SceneManager.LoadScene(1);
        }
//        Debug.LogError(UserData.UserName);
    }
}

public class Data
{
    public string id;
    public string email;
    public string userName;
    public string duel;
    public int gold;
    public int cristal;
    public string hiro;
    public string inventory;
    public string otherData;
    public string chestTime;
    public string index;
}