﻿using UnityEngine;
using UnityEngine.UI;

public class UserNamePanel : MonoBehaviour
{
    [SerializeField] private Text setName, nameDiscr, resume, userName;
    public string GetName()
    {
        return GetComponentInChildren<InputField>().text;
    }

    private void OnEnable()
    {
        setName.text=Config.Constant.SET_NAME[Config.CURRENT_LANGUAGE];
        nameDiscr.text=Config.Constant.DISC_ON_SET_NAME[Config.CURRENT_LANGUAGE];
        resume.text=Config.Constant.RESUME[Config.CURRENT_LANGUAGE];
        userName.text=Config.Constant.USER_NAME[Config.CURRENT_LANGUAGE];
    }
}
