﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnNav : MonoBehaviour
{
    public Image icon, bg;
    public Text btnTxt;
    public bool isMess;

    public bool IsMess
    {
        get => isMess;
        set
        {
             isMess = value;
             mess.SetActive(value);
        }
    }

    public void TurnUIMess(bool check)
    {
        if (!check&&IsMess)
        {
            mess.SetActive(false);
        }
        else if (check&&IsMess)
        {
            IsMess = true;
        }
           
    }

    public GameObject mess;
    public Text messCountTxt;
    public int index;
    public GameObject panel;
    

    public void SetText(int _count)
    {
        messCountTxt.text = _count.ToString();
    }
}