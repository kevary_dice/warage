﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemPrefab : MonoBehaviour
{
    [SerializeField] private Image itemIcon;
    [SerializeField] private Image border;
    [SerializeField] private Image selectedImage;
    [SerializeField] private Color standartColor;
    [SerializeField] private Text countStack;
    [SerializeField] private int count = 1;
    [SerializeField] private Config config;
    [SerializeField] private RawImage rarityBg;

    public RawImage RarityBg => rarityBg;

    private Shop shop;
[SerializeField]
    private Item _currentItem;

    public Item CurrentItem
    {
        get => _currentItem;
        set => _currentItem = value;
    }

    private void Start()
    {
        shop = FindObjectOfType<Shop>();
    }

    public void SetIcon(Item item)
    {
        _currentItem = item;
        itemIcon.sprite = item.ItemIcon;
        try
        {
            ItemEquip itemEquip = item as ItemEquip;
//            Debug.LogError((int)itemEquip.rarity);
            
                rarityBg.color = config.rarityColor[(int) itemEquip.rarity];
        }
        catch (Exception e)
        {
            try
            {
                ItemWeapon itemEquip = item as ItemWeapon;
                
                    rarityBg.color = config.rarityColor[(int) itemEquip.rarity];
            }
            catch (Exception exception)
            {
               
                    rarityBg.color = standartColor;
            }
        }
    }

    public void ShowTooltype()
    {
        shop.ShowTooltype(this,count);
    }

    private void OnDestroy()
    {
        Destroy(gameObject);
    }

    public void SetCount(int i)
    {
        count = i;
        if (i == 1)
        {
            countStack.text = "";
        }
        else countStack.text = "x" + i;
    }
}