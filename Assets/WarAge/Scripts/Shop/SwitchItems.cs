﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchItems : MonoBehaviour
{
   [SerializeField] private EquipWeaponType type;
   [SerializeField] private bool isSell;
   private Shop _shop;
   private void Start()
   {
      _shop = FindObjectOfType<Shop>();
      GetComponent<Button>().onClick.AddListener(Click);
   }

   public void Click()
   {
      if (!isSell)
      {
         _shop.ShowPanel(type);
      }
      else
      {
         _shop.ShowPanel();
      }
      
   }
}
