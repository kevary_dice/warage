﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Purchase : MonoBehaviour
{
    [SerializeField] private GameObject panelLoading;
    [SerializeField]
    private Text duelTxt, goldTxt, cristalTxt;
    [SerializeField] private Config _config;

    public Config Config => _config;

    public Purchase(){}

   private void Awake()
   {
      UpdateResourse();
//       DontDestroyOnLoad(this);
   }

   public bool PurchaseReturn(CharacterData.Currency currency, int price)
    {

        switch (currency)
        {
            case CharacterData.Currency.Gold:
                return PurchaseForGold(price);
                break;
            case CharacterData.Currency.Crystal:
                return PurchaseForCristal(price);
                break;
            default:
                throw new NotSupportedException();
        }
    }

   public bool CheckPurchase(CharacterData.Currency currency, int price)
   {
       switch (currency)
       {
           case CharacterData.Currency.Gold:
               if (UserData.Gold >= price)
               {
                   return true;
               }
               else return false;
               
           case CharacterData.Currency.Crystal:
               if (UserData.Cristal >= price)
               {
                   return true;
               }
               else return false;
           default:
               throw new NotSupportedException();
       }
   }

    public bool PurchaseForGold(int price)
    {
        if (UserData.Gold >= price)
        {
            UserData.Gold -= price;
            UpdateResourse();
            //покупка
            StartCoroutine(SendGold(price));
            return true;
        }

        // надо сообщить игроку о том что нехватает валюты
        return false;
    }

    public bool PurchaseForCristal(int price)
    {
        if (UserData.Cristal >= price)
        {
            UserData.Cristal -= price;
            UpdateResourse();
            StartCoroutine(SendCrystal(price));
            return true;
        }

        return false;
    }

    public void UpdateResourse()
    {
        duelTxt.text = UserData.CurrentDuel + "\\" + UserData.MaxDuel;
        goldTxt.text = UserData.Gold.ToString();
        cristalTxt.text = UserData.Cristal.ToString();
    }

     IEnumerator SendGold(int gold)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("gold", gold);
        form.AddField("action", "setGold");
        WWW www = new WWW(Config.url, form);
        yield return www;
        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }

     IEnumerator SendCrystal(int diamond)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("crystal", diamond);
        form.AddField("action", "setCrystal");
        WWW www = new WWW(Config.url, form);
        yield return www;
        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }
}