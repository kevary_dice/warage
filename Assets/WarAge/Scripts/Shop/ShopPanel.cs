﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ShopPanel : MonoBehaviour
{
    [SerializeField] private Config _config;
    [SerializeField] private Text _textName;
    
    [SerializeField] private GameObject _itemPrefabe,panelLoading;
    [SerializeField] private Transform _parentItemPrefabe;
    private List<Inventory> items;
    [SerializeField] private Purchase _purchase;
    [SerializeField] private Text back;
    [SerializeField] private TooltypeAcceptBuy _tooltypeAcceptBuy;
    public Text TextName => _textName;

    public void SetItems(List<Item> items)
    {
        SwitchName(items[0].equipWeaponType);

        foreach (var VARIABLE in items)
        {
            GameObject _gameObject = Instantiate(_itemPrefabe, _parentItemPrefabe);
            _gameObject.GetComponent<ItemPrefab>().SetIcon(VARIABLE);
            if (VARIABLE.itemTyepe==Item.ItemType.Сonsumables)
            {
                GameObject _gameObjectX10 = Instantiate(_itemPrefabe, _parentItemPrefabe);
                _gameObjectX10.GetComponent<ItemPrefab>().SetIcon(VARIABLE);
                _gameObjectX10.GetComponent<ItemPrefab>().SetCount(10);
            }
            
        }
    }

    public void SetItems(List<Inventory> items)
    {
        this.items = items;
        SetName(Config.Constant.SEND[Config.CURRENT_LANGUAGE]);
        StartCoroutine(GetInventory());
    }

    private void OnDisable()
    {
        foreach (var VARIABLE in _parentItemPrefabe.GetComponentsInChildren<ItemPrefab>())
        {
            Destroy(VARIABLE.gameObject);
        }
    }

    public void SwitchName(EquipWeaponType type)
    {
        switch (type)
        {
            case EquipWeaponType.Сonsumables:

            {
                SetName(Config.Constant.POTION[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Boots:

            {
                SetName(Config.Constant.BOOTS[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Bow:

            {
                SetName(Config.Constant.BOW_WEAPON[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Gloves:

            {
                SetName(Config.Constant.GLOVES[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Helmet:

            {
                SetName(Config.Constant.HELMET[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Magic:

            {
                SetName(Config.Constant.MAGIC_WEAPON[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Melee:

            {
                SetName(Config.Constant.MELEE_WEAPON[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Neclace:

            {
                SetName(Config.Constant.NECLEACE[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Ring:

            {
                SetName(Config.Constant.RING[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.ChestBody:

            {
                SetName(Config.Constant.ARMOR[Config.CURRENT_LANGUAGE]);
                break;
            }

            case EquipWeaponType.Shield:

            {
                SetName(Config.Constant.SHIELD[Config.CURRENT_LANGUAGE]);
                break;
            }

            default: throw new NotImplementedException();
        }
    }

    private void SetName(string text)
    {
        _textName.text = text;
        back.text=Config.Constant.BACK[Config.CURRENT_LANGUAGE];
    }

    public void UpdateInventory()
    {
        StartCoroutine(GetInventory());
    }
    
    public object Purchase(ItemPrefab currentItem,int count=1)
    {
        if (TextName.text == Config.Constant.SEND[Config.CURRENT_LANGUAGE])
        {
            _purchase.PurchaseReturn(CharacterData.Currency.Gold,- currentItem.CurrentItem.ItemSellPrice*count);
            UserData.RemoveItem(new Inventory()
            {
                itemId = _config.AllItem.IndexOf(_config.AllItem.Find(x => x.ItemName == currentItem.CurrentItem.ItemName)),
                ItemCount = count
            });
            StartCoroutine(SetInventory(JsonUtility.ToJson(UserData.Inventory)));
            Destroy(currentItem);
        }
        else if (!_purchase.PurchaseReturn(CharacterData.Currency.Gold, currentItem.CurrentItem.ItemBuyPrice*count))
        {
            _tooltypeAcceptBuy.gameObject.SetActive(true);
            _tooltypeAcceptBuy.SetText(false);
        }
        else
        {
            Debug.LogError("count"+count);
            Inventory inventory = new Inventory()
            {
                itemId = _config.AllItem.IndexOf(_config.AllItem.Find(x => x.ItemName == currentItem.CurrentItem.ItemName)),
                ItemCount = count
            };
            
            UserData.AddInventory(inventory);

            StartCoroutine(SetInventory(JsonUtility.ToJson(UserData.Inventory)));
            
        }

        return null;
    }
    IEnumerator SetInventory(string json)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("inventory", json);
        form.AddField("action", "setInventory");
        WWW www = new WWW(Config.url, form);
        yield return www;
   

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
      
    }
    public IEnumerator GetInventory()
    {
        panelLoading.SetActive(true);
        OnDisable();
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        Debug.LogError(UserData.Id);
        form.AddField("action", "getInventory");

        WWW www = new WWW(Config.url, form);
        yield return www;
        UserData.InventoryToJson(www.text);
        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);

        Debug.LogError(www.text);
       
        foreach (var VARIABLE in UserData.Inventory.items)
        {
            if (_config.AllItem[VARIABLE.itemId].isStacable)
            {
                GameObject _gameObject = Instantiate(_itemPrefabe, _parentItemPrefabe);
                _gameObject.GetComponent<ItemPrefab>().SetIcon(_config.AllItem[VARIABLE.itemId]);
                _gameObject.GetComponent<ItemPrefab>().SetCount(VARIABLE.ItemCount);
            }
            else
            {
                for (int i = 0; i < VARIABLE.ItemCount; i++)
                {
                    GameObject _gameObject = Instantiate(_itemPrefabe, _parentItemPrefabe);
                    _gameObject.GetComponent<ItemPrefab>().SetIcon(_config.AllItem[VARIABLE.itemId]);
                }
            }
            
        }
        panelLoading.SetActive(false);
    }
}