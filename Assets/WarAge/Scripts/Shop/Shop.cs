﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Shop : MonoBehaviour
{
   [SerializeField] private Config _config;
   [SerializeField] private GameObject menuMain;
   [SerializeField] private GameObject menuWeapon;
   [SerializeField] private GameObject menuArmor;
   [SerializeField] private ShopPanel shopPanel;
   [SerializeField] private TooltypeItem _tooltypeItem;
   [SerializeField] private Text shopSmyth,send, potion, weapon, armorAndShield, neclace, ring,melee,magic,bow,titleWeapon,backWeapon
      , armor,gloves,boots,shield,backArmor,helmet,armorAndShieldTitle;
   private void OnDisable()
   {
     OffAll();
   }

   private void OnEnable()
   {
      shopSmyth.text=Config.Constant.SHOP_BLACKSMITH[Config.CURRENT_LANGUAGE];
      send.text=Config.Constant.SEND[Config.CURRENT_LANGUAGE];
      potion.text=Config.Constant.POTION[Config.CURRENT_LANGUAGE];
      weapon.text=Config.Constant.WEAPON[Config.CURRENT_LANGUAGE];
      armorAndShield.text=Config.Constant.ARMOR_AND_SHIELD[Config.CURRENT_LANGUAGE];
      neclace.text=Config.Constant.NECLEACE[Config.CURRENT_LANGUAGE];
      ring.text=Config.Constant.RING[Config.CURRENT_LANGUAGE];
      melee.text=Config.Constant.MELEE_WEAPON[Config.CURRENT_LANGUAGE];
      magic.text=Config.Constant.MAGIC_WEAPON[Config.CURRENT_LANGUAGE];
      bow.text=Config.Constant.BOW_WEAPON[Config.CURRENT_LANGUAGE];
      backWeapon.text = Config.Constant.BACK[Config.CURRENT_LANGUAGE];
      titleWeapon.text = Config.Constant.WEAPON_TITLE[Config.CURRENT_LANGUAGE];
      armor.text = Config.Constant.ARMOR[Config.CURRENT_LANGUAGE];
      gloves.text = Config.Constant.GLOVES[Config.CURRENT_LANGUAGE];
      boots.text = Config.Constant.BOOTS[Config.CURRENT_LANGUAGE];
      shield.text = Config.Constant.SHIELD[Config.CURRENT_LANGUAGE];
      backArmor.text = Config.Constant.BACK[Config.CURRENT_LANGUAGE];
      armorAndShieldTitle.text = Config.Constant.ARMOR_AND_SHIELD[Config.CURRENT_LANGUAGE];
      helmet.text = Config.Constant.HELMET[Config.CURRENT_LANGUAGE];
      
      OffAll();
      menuMain.SetActive(true);
   }

   public void ShowPanel(EquipWeaponType type)
   {
      shopPanel.SetItems(_config.AllItem.FindAll(x=>x.equipWeaponType==type));
   }
   
   public void ShowPanel()
   {
      
      shopPanel.SetItems(UserData.Inventory.items);
   }
   public void OffAll()
   {
      List<GameObject> _gameObjects=new List<GameObject>(){menuArmor,menuMain,menuWeapon,shopPanel.gameObject};
      foreach (var VARIABLE in _gameObjects)
      {
         VARIABLE.SetActive(false);
      }
   }

   public void ShowTooltype(ItemPrefab _currentItem,int count)
   {
     
      _tooltypeItem.gameObject.SetActive(true);
      _tooltypeItem.SetParametrs(_currentItem,count);
   }
}
