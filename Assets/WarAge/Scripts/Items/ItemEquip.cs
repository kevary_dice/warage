﻿using UnityEngine;

[CreateAssetMenu(menuName = "WarAge/Inventory/NewEquip", fileName = "NewEquip")]

public class ItemEquip : Item
{
    public int NeededLevel, NeededSlot;

    
    public Rarity rarity;


    [Header("Добавляет герою")]
    public float addForce;
    public float addAgility, addMind, addPAttack, addMAttack, addArmor, AddSpdAttack, addMoveSpeed, addMaxHp, addMaxMp;
    
}
