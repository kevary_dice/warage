﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "WarAge/Inventory/NewConsumables", fileName = "NewConsumables")]
public class ItemConsumables : Item
{
    public float addHp, addMp;

}
