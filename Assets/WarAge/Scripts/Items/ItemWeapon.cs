﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Rarity {Normal=0, Great, Mystic, Legendary}
[CreateAssetMenu(menuName = "WarAge/Inventory/NewWeapon", fileName = "NewWeapon")]

public class ItemWeapon : Item
{
    public int NeededLevel;
    

    public Rarity rarity;
    [Tooltip("0 дальний бой 1 ближний бой 2 маг")]
    public int hiroType = 0;
    public bool isTwoHeand;
    [Header("Добавляет герою")]
    public float addForce;
    public float addAgility, addMind, addPAttack, addMAttack, addArmor, AddSpdAttack, addMoveSpeed, addMaxHp, addMaxMp;
    public GameObject bulletsPrefab;
}
