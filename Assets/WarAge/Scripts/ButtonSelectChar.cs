﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSelectChar : MonoBehaviour
{
    public void ChangeText(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }
}
