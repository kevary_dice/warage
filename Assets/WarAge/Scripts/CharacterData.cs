﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "WarAge/NewCharacter", fileName = "NewChar")]
public class CharacterData : ScriptableObject
{
    public int id;
    public string CharName;
    public List<string> CharNames;
    [Multiline(4)] public string CharDisc;
    public List<string> CharDiscs;
    public int lvl, hp, mp, maxHp, maxMp;
    [SerializeField] private int _currentExp;
    public float force, agility, mind, pAttack, mAttack, armor, spdAttack, moveSpeed;

    public float Force
    {
        get => force;
        set
        {
            pAttack += (value - force) * 5;
            maxHp += (int) (value - force) * 25;
            force += value - force;
        }
    }


    public float Agility
    {
        get => agility;
        set
        {
            moveSpeed += 0.5f * (value - agility);
            spdAttack += 0.5f * (value - agility);
            agility += value - agility;
        }
    }

    public float Mind
    {
        get => mind;
        set
        {
            maxMp += 30 * (int) (value - mind);
            mAttack += 5 * (value - mind);
            mind += value - mind;
        }
    }

    [SerializeField] private ItemPointUpgrade itemUpgrade;
    [SerializeField] private PointUpgrade pointUpgrade;

    public ItemPointUpgrade ItemUpgrade
    {
        get => itemUpgrade;
        set { itemUpgrade = value; }
    }

    public PointUpgrade PointUpgrade
    {
        get => pointUpgrade;
        set
        {
//            Debug.LogError("test");
//            pointUpgrade = value;
            UpgradePoint(value);
        }
    }

    public Sprite CharIcon, CharTypeIcon;
    public GameObject PrefabMenu, PrefabGame;

    public enum AttackType
    {
        Randged,
        Melee,
        Magic
    }

    public AttackType attackType;
    public int buyPrice;
    public bool isBuyabled;

    public enum Currency
    {
        Gold,
        Crystal
    };

    public Currency currency;
    public SkillData[] CharSkills = new SkillData[10];
    public Config config;
    public int currentPoint;

    public int CurrentPoint
    {
        get => currentPoint;
        set
        {
            currentPoint = value;
            CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
            try
            {
                charId.CharIdLvls.Find(x => x.current).point.currentPoint = value;
            }
            catch (Exception e)
            {
            }

            UserData.Hiro = JsonUtility.ToJson(charId);
        }
    }

    [SerializeField] private int _countSkillPoint;


    public int CountSkillPoint
    {
        get => _countSkillPoint;
        set { _countSkillPoint = value; }
    }

    public void UpgradeItem(ItemPointUpgrade upgrade)
    {
        Force += upgrade.force;
        Agility += upgrade.agility;
        Mind += upgrade.inteligence;
        mAttack += upgrade.mAtack;
        pAttack += upgrade.pAtack;
        moveSpeed += upgrade.speed;
        spdAttack += upgrade.spdAttack;
        maxHp += (int) upgrade.maxHp;
        maxMp += (int) (upgrade.maxMp);
        armor += upgrade.armor;


        itemUpgrade.force += upgrade.force;
        itemUpgrade.agility += upgrade.agility;
        itemUpgrade.inteligence += upgrade.inteligence;
        itemUpgrade.mAtack += upgrade.mAtack;
        itemUpgrade.pAtack += upgrade.pAtack;

        itemUpgrade.speed += upgrade.speed;
        itemUpgrade.spdAttack += upgrade.spdAttack;
        itemUpgrade.maxHp += upgrade.maxHp;
        itemUpgrade.maxMp += upgrade.maxMp;
        itemUpgrade.armor += upgrade.armor;

        CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        try
        {
            charId.CharIdLvls.Find(x => x.current).itemUpgrade = itemUpgrade;
        }
        catch (Exception e)
        {
        }

        UserData.Hiro = JsonUtility.ToJson(charId);
    }

    public void UpgradePoint(PointUpgrade upgrade)
    {
//        Debug.LogError(upgrade);
        Force += upgrade.force - pointUpgrade.force;
        Agility += upgrade.agility - pointUpgrade.agility;
        Mind += upgrade.inteligence - pointUpgrade.inteligence;
        CurrentPoint = lvl - ((int) upgrade.force + (int) upgrade.agility + (int) upgrade.inteligence) ;
        Debug.LogError(CurrentPoint);
        pointUpgrade = upgrade;
    }

    public void Purchase()
    {
        isBuyabled = true;
    }

    public int CurrentExp
    {
        get => _currentExp;
        set
        {
            if (value >= config.expForLvl[lvl])
            {
                LvlUp();
                CurrentExp = value;
            }
            else
            {
                _currentExp = value;

                CharIdLvlList charId = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
                try
                {
                    charId.CharIdLvls.Find(x => x.current).exp = _currentExp;
                }
                catch (Exception e)
                {
                }

                UserData.Hiro = JsonUtility.ToJson(charId);
            }
        }
    }

    public void LvlUp()
    {
        lvl++;
        CurrentPoint++;
        _countSkillPoint++;
    }

    public void RestoreHp(int count)
    {
        if (hp + count > maxHp)
        {
            hp = maxHp;
        }
        else
        {
            hp += count;
        }
    }

    public void RestoreMp(int count)
    {
        if (mp + count > maxMp)
        {
            mp = maxMp;
        }
        else
        {
            mp += count;
        }
    }
}

[Serializable]
public class CharIdLvl
{
    public int id;
    public int exp;
    public bool current;
    public SkillLvlUpList skills;
    public PointLvlUp point;
    public List<int> itemsId;
    public ItemPointUpgrade itemUpgrade;
    public PointUpgrade pointUpgrade;

    public CharIdLvl()
    {
        point = new PointLvlUp();
        skills = new SkillLvlUpList();
    }
}

[Serializable]
public class CharIdLvlList
{
    public List<CharIdLvl> CharIdLvls;

    public CharIdLvlList()
    {
        CharIdLvls = new List<CharIdLvl>();
    }
}

[Serializable]
public class PointLvlUp
{
    public int currentPoint;
    public float pointAgility, pointStrenge, pointInteligence;

    public override string ToString()
    {
        return pointAgility.ToString() + "= agility  " + pointInteligence.ToString() + "=inteligence  " +
               pointStrenge.ToString() + "= strenge";
    }
}

[Serializable]
public class SkillLvlUpList
{
    public List<SkillLvlUp> SkillsLvlUp;

    public SkillLvlUpList()
    {
        SkillsLvlUp = new List<SkillLvlUp>();
        for (int i = 0; i < 10; i++)
        {
            SkillsLvlUp.Add(new SkillLvlUp() {skillId = i, skillLvl = 0});
        }
    }
}

[Serializable]
public class SkillLvlUp
{
    public int skillId;
    public int skillLvl;
}

[Serializable]
public class PointUpgrade
{
    public float inteligence;
    public float agility;
    public float force;

    public PointUpgrade(float force, float agility, float inteligence)
    {
        this.inteligence = inteligence;
        this.force = force;
        this.agility = agility;
    }

    public override string ToString()
    {
        return "inteligense = " + inteligence + " agility = " + agility + " force = " + force;
    }
}

[Serializable]
public class ItemPointUpgrade
{
    public float inteligence, agility, force, pAtack, mAtack, armor, spdAttack, speed, maxMp, maxHp;


    public ItemPointUpgrade(float inteligence, float agility, float force, float pAtack, float mAtack, float armor,
        float spdAttack, float speed, float maxMp, float maxHp)
    {
        this.inteligence = inteligence;
        this.agility = agility;
        this.force = force;
        this.pAtack = pAtack;
        this.mAtack = mAtack;
        this.armor = armor;
        this.spdAttack = spdAttack;
        this.speed = speed;
        this.maxMp = maxMp;
        this.maxHp = maxHp;
    }

    public ItemPointUpgrade()
    {
    }

    public override string ToString()
    {
        return "inteligense = " + inteligence + " agility = " + agility + " force = " + force;
    }
}