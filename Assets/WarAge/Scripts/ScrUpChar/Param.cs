﻿using UnityEngine;
using UnityEngine.UI;
public enum  Test{Force,Agility,Inteligence}

public class Param : MonoBehaviour
{
    [SerializeField] private Test type;
    [SerializeField] private Text countText;
    [SerializeField] private UpCharacter _upCharacter;
    [SerializeField] private float count;
    [SerializeField]
    private float tempPoint;

    [SerializeField] private float newPoint;

    public float NewPoint => newPoint;

    public float TempPoint => tempPoint;

    public float Count
    {
        get => count;
        set
        {
            count = value;
            countText.text = value.ToString();
            
        }
    }

//    public float newPoint => Mathf.Abs(TempPoint- Count) ;

    public void SetParametrs(float param,float point)
    {
        Count = param;
        tempPoint = point;
        newPoint = 0;
    }

    public void ChangeParam(int _count)
    {
        if (_count > 0 && _upCharacter.CountPoint > 0)
        {
            _upCharacter.CountPoint--;
            Count++;
            tempPoint++;
            newPoint++;

        }
        else if (_count < 0 && ( TempPoint >0) )
        {
            Debug.LogError("Уменьшаю");
            _upCharacter.CountPoint++;
            Count--;
            tempPoint--;
            newPoint--;
        }
        else
        {
            Debug.Log("нет поинтов");
        }
        _upCharacter.UpdatePoint();
    }

    public void UpdatePoint()
    {
        
    }
    

    public bool CheckPoint()
    {
        switch (type)
        {
            case Test.Force:
            {
                
                    return CheckForcePointZero();
               
               
            }
            case Test.Agility:
            {
                
                    return CheckAgilityPointZero();
               
            }
            case Test.Inteligence:
            {
                
                    return CheckInteligencePointZero();
                
            }
            default: return false;

        }
    }
    public bool CheckForcePointZero()
    {
        if (_upCharacter.CharacterData.PointUpgrade.force < 1)
        {
            return false;
        }

        return true;
    }
    public bool CheckInteligencePointZero()
    {
        if (_upCharacter.CharacterData.PointUpgrade.inteligence < 1)
        {
            return false;
        }

        return true;
    }
    public bool CheckAgilityPointZero()
    {
        if (_upCharacter.CharacterData.PointUpgrade.agility < 1)
        {
            return false;
        }

        return true;
    }
}