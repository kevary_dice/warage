﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UpCharacter : MonoBehaviour
{
    [SerializeField] private GameObject panelLoading;
    [SerializeField] private Text countUpgrade,pAtack,mAtack,speed,atackSpeed,maxHp,maxMp,btnOk,title,titleDisc,force,agility,inteligence,
        textPAttack,textSpeed,textMAttack,textMaxHp,textASpeed,textMaxMp,AVAILABLE,update;
    [SerializeField] private Param countStrength, countAgility, countMaind;
    [SerializeField] private int countPoint;
    [SerializeField] private CharacterData _characterData;
    [SerializeField] private MainScreen _mainScreen;
    [SerializeField] private Navigation _navigation;
    public CharacterData CharacterData => _characterData;

    [SerializeField] private ScrnInvenory _scrnInvenory;
    [SerializeField] private bool isInventory;


    public int CountPoint
    {
        get => countPoint;
        set
        { 
            countPoint = value;
            countUpgrade.text = value.ToString();
        }
    }

    [SerializeField] private PointLvlUp _pointLvlUp;


    private void OnEnable()
    {
        _characterData = FindObjectOfType<CurrentCharacter>().CharacterData;
        _pointLvlUp = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro).CharIdLvls.Find(x => x.current).point;
        CountPoint = _pointLvlUp.currentPoint;
        countStrength.SetParametrs(_characterData.Force,_characterData.PointUpgrade.force);
        countAgility.SetParametrs(_characterData.agility,_characterData.PointUpgrade.agility);
        countMaind.SetParametrs(CharacterData.mind,_characterData.PointUpgrade.inteligence);
        UpdatePoint();
    }

    public void AplyChanges()
    {
        
        _pointLvlUp.pointStrenge = countStrength.Count;
        _pointLvlUp.pointAgility=countAgility.Count;
        _pointLvlUp.pointInteligence=countMaind.Count;
        _pointLvlUp.currentPoint = CountPoint;
        
        _characterData.currentPoint = CountPoint;
        _characterData.UpgradePoint(new PointUpgrade(countStrength.TempPoint,countAgility.TempPoint,countMaind.TempPoint)); 
        
//        _characterData.force = countStrength.Count;
//        _characterData.agility =countAgility.Count;
//        _characterData.mind= countMaind.Count;
        
        CharIdLvlList charIdLvlList = JsonUtility.FromJson<CharIdLvlList>(UserData.Hiro);
        charIdLvlList.CharIdLvls.Find(x => x.current).point = _pointLvlUp;
        charIdLvlList.CharIdLvls.Find(x => x.current).pointUpgrade = _characterData.PointUpgrade;
//        Debug.LogError(JsonUtility.ToJson(charIdLvlList));
        UserData.Hiro = JsonUtility.ToJson(charIdLvlList);
        _mainScreen.OnEnable();
        StartCoroutine(SendChar(UserData.Hiro));
        _navigation.UpdateImage();

    }

    public void UpdatePoint()
    {
        btnOk.text=Config.Constant.ACCEPT[Config.CURRENT_LANGUAGE];
        title.text=Config.Constant.YOU_CAN_UPDATE_CHARACTER[Config.CURRENT_LANGUAGE];
        titleDisc.text=Config.Constant.DISK_UPDATE[Config.CURRENT_LANGUAGE];
        force.text=Config.Constant.FORCE[Config.CURRENT_LANGUAGE];
        agility.text=Config.Constant.AGILITY[Config.CURRENT_LANGUAGE];
        inteligence.text=Config.Constant.INTELLIGENCE[Config.CURRENT_LANGUAGE];
        textPAttack.text=Config.Constant.P_ATTACK[Config.CURRENT_LANGUAGE];
        textSpeed.text=Config.Constant.SPEED[Config.CURRENT_LANGUAGE];
        textMAttack.text=Config.Constant.M_ATTACK[Config.CURRENT_LANGUAGE];
        textMaxHp.text=Config.Constant.MAX_HP[Config.CURRENT_LANGUAGE];
        textASpeed.text=Config.Constant.ATTACK_SPEED[Config.CURRENT_LANGUAGE];
        textMaxMp.text=Config.Constant.MAX_MP[Config.CURRENT_LANGUAGE];
        AVAILABLE.text=Config.Constant.AVAILABLE[Config.CURRENT_LANGUAGE];
        update.text=Config.Constant.UPDATES[Config.CURRENT_LANGUAGE];
        
        maxHp.text =(_characterData.maxHp+  countStrength.NewPoint * 25) .ToString();
        maxMp.text =(_characterData.maxMp+ countMaind.NewPoint * 30) .ToString();
        pAtack.text =(_characterData.pAttack+ countStrength.NewPoint * 5) .ToString();
        mAtack.text =(_characterData.mAttack+ countMaind.NewPoint * 5) .ToString();
        speed.text =(_characterData.moveSpeed+ countAgility.NewPoint * .5f) .ToString();
        atackSpeed.text =(_characterData.spdAttack+ countAgility.NewPoint * .5f) .ToString();
    }

    public void SetInventory()
    {
        isInventory = true;
    }
    public IEnumerator SendChar(string json)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;
        
        Debug.LogError("Вставил в базу");

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        Exit();
        panelLoading.SetActive(false);
    }

    public void Exit()
    {
        if (isInventory)
        {
            _scrnInvenory.CheckChar();
            _scrnInvenory.UpdateText();
            isInventory = false;
        }
        gameObject.SetActive(false);
    }
}