﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyBattlePass : MonoBehaviour
{
    [SerializeField] private Text duel;
    [SerializeField] private int cost;
    [SerializeField] private int addBattlePass;
    
    [SerializeField] private CharacterData.Currency _currencyCost = CharacterData.Currency.Crystal;
    [SerializeField] private Purchase _purchase;
    [SerializeField] private TooltypeAcceptBuy _tooltypeAcceptBuy;

    private void Awake()
    {
       
        _purchase = FindObjectOfType<Purchase>();
        GetComponent<Button>().onClick.AddListener(ByeGold);
    }

    public void ByeGold()
    {
        _tooltypeAcceptBuy.gameObject.SetActive(true);
        _tooltypeAcceptBuy.SetText(_purchase.CheckPurchase(_currencyCost, cost),addBattlePass);
        if (_purchase.PurchaseReturn(_currencyCost, cost))
        {

            StartCoroutine(UpdateDuel(addBattlePass));
        }
    }
    public IEnumerator UpdateDuel(int value)
    {
//        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("userId", UserData.Id);
        form.AddField("value", value);
        form.AddField("action", "sendDuel");
        WWW www = new WWW(Config.url, form);
        yield return www;
//        Debug.LogError(www.text);
        Duel _duel = JsonUtility.FromJson<Duel>(www.text);
        Debug.LogError(_duel.duel[0]);
        if (_duel.duel[0] >= 0)
        {
            UserData.DuelJson = www.text;
            duel.text = UserData.CurrentDuel + "\\" + UserData.MaxDuel;
//            check = true;
        }
        else
        {
//            check = false;
        }
//        panelLoading.SetActive(false);
    }
}
