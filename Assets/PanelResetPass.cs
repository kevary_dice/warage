﻿using System;

using UnityEngine;
using UnityEngine.UI;

public class PanelResetPass : MonoBehaviour
{
    [SerializeField] Text back,resetPass,returnAcces,or,enterGoogle,enterFacebook;
    private void OnEnable()
    {
        back.text = Config.Constant.BACK[Config.CURRENT_LANGUAGE];
        resetPass.text = Config.Constant.RESET_PASS[Config.CURRENT_LANGUAGE];
        returnAcces.text = Config.Constant.RETURN_ACCESS[Config.CURRENT_LANGUAGE];
        or.text = Config.Constant.OR[Config.CURRENT_LANGUAGE];
        enterGoogle.text = Config.Constant.ENTER_GOOGLE[Config.CURRENT_LANGUAGE];
        enterFacebook.text = Config.Constant.ENTER_FACEBOOK[Config.CURRENT_LANGUAGE];
    }
}
