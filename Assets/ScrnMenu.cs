﻿
using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class ScrnMenu : MonoBehaviour
{
    [SerializeField] private GameObject panelLoading;
    [SerializeField] private int exp;
    [SerializeField] private Text resume, camp, settings, contactWithUs, exit;

    private void OnEnable()
    {
        resume.text = Config.Constant.RESUME[Config.CURRENT_LANGUAGE];
        camp.text = Config.Constant.TRAIN_CAMP[Config.CURRENT_LANGUAGE];
        settings.text= Config.Constant.SETTINGS[Config.CURRENT_LANGUAGE];
        contactWithUs.text= Config.Constant.FEEDBACK[Config.CURRENT_LANGUAGE];
        exit.text= Config.Constant.EXIT[Config.CURRENT_LANGUAGE];
    }

    public void AddExpForLvl()
    {
        FindObjectOfType<CurrentCharacter>().CharacterData.CurrentExp += exp;
        StartCoroutine(SendChar(UserData.Hiro));
    }
    public IEnumerator SendChar(string json)
    {
        panelLoading.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("id", UserData.Id);
        form.AddField("json", json);
        form.AddField("action", "setChar");
        WWW www = new WWW(Config.url, form);
        yield return www;

        if (www.error != null)
            Debug.LogError("Ошибка " + www.error);
        panelLoading.SetActive(false);
    }
}
