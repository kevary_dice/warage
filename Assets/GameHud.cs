﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameHud : MonoBehaviour
{
    public Slider hp, mp;
    public Text txtHp, txtMp, lvl;
    public Image charIcon;

    public RectTransform skillBar, buffContainer;
    public GameObject buffIconPrefab, activSkillPrefab;


    // controll
    public CameraController camContrll;
    public PlayerController playerControll;
    public GameObject controll;

    public void SetupHud(CharacterData charData)
    {
       
        hp.maxValue = charData.maxHp;
        hp.value = charData.hp;
        mp.maxValue = charData.maxMp;
        mp.value = charData.mp;
        txtHp.text = charData.hp + "/" + charData.maxHp;
        txtMp.text = charData.mp + "/" + charData.maxMp;
        lvl.text = charData.lvl.ToString();
        charIcon.sprite = charData.CharIcon;

        for (int i = 0; i < charData.CharSkills.Length; i++)
        {
            if (charData.CharSkills[i].isInvestigated)
            {

                if (charData.CharSkills[i].skillType == SkillData.SkillType.Passive)
                {
                    Image buffIcon = Instantiate(buffIconPrefab, buffContainer).GetComponent<Image>();
                    buffIcon.sprite = charData.CharSkills[i].SkillIcon;
                }
                else
                {
                    ActiveSkillBtn btnSkill = Instantiate(activSkillPrefab, skillBar).GetComponent<ActiveSkillBtn>();
                    btnSkill.icon.sprite = charData.CharSkills[i].SkillIcon;
                    btnSkill.kd_txt.text = "";
                    btnSkill.kd_img.fillAmount = 0;
                }
            }
        }
        
        Vector2 SkilBarPos = new Vector2(-445f, 0f);
        if (PlayerPrefs.GetInt(UserData.Id + "SkilPos") == 1)
        {
            SkilBarPos = new Vector2(445f, 0f);
        }
        skillBar.localPosition = SkilBarPos;


    }

}
