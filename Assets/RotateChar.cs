﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RotateChar : MonoBehaviour,IDragHandler
{
    [SerializeField] private GameObject charContainer;
      public void OnDrag(PointerEventData eventData)
       {
          
           charContainer.transform.Rotate(charContainer.transform.rotation.x,
               charContainer.transform.rotation.y - eventData.delta.x*0.5f , 0);
       }

   
}
