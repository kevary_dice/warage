﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WorldMap : MonoBehaviour
{
    public Image[] allFlags;
    public Sprite flagActive, flagComplite;
    public Vector2[] mapAutoScroll;
    public int currenMap;
    [SerializeField] private Text back;
    [SerializeField] private Transform container;


    private void OnEnable()
    {
        AutoScroll();
        back.text = Config.Constant.BACK[Config.CURRENT_LANGUAGE];
    }
    public void AutoScroll()
    {
        
        for (int i = 0; i < allFlags.Length; i++)
        {
            allFlags[i].enabled = true;
            if(i > currenMap)
            {
                allFlags[i].enabled = false;
            }
            else if(i == currenMap)
            {
                allFlags[i].sprite = flagActive;
            }
            else
            {
                allFlags[i].sprite = flagComplite;
            }
        }
        container.localPosition = mapAutoScroll[currenMap];
    }
}
