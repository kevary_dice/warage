﻿using UnityEngine;
using UnityEngine.UI;

public class PanelSignUp : MonoBehaviour
{
    [SerializeField] private Text backToAut, yourEmail, newPass, repeatPass, acceptPrivacy, reg, or;
    private void OnEnable()
    {
        backToAut.text = Config.Constant.BACK_TO_AUT[Config.CURRENT_LANGUAGE];
        yourEmail.text = Config.Constant.YOUR_EMAIL[Config.CURRENT_LANGUAGE];
        newPass.text = Config.Constant.NEW_PASS[Config.CURRENT_LANGUAGE];
        repeatPass.text = Config.Constant.REPEAT_PASS[Config.CURRENT_LANGUAGE];
        acceptPrivacy.text = Config.Constant.ACCEPT_PRIVACY[Config.CURRENT_LANGUAGE];
        reg.text = Config.Constant.REG[Config.CURRENT_LANGUAGE];
        or.text = Config.Constant.OR[Config.CURRENT_LANGUAGE];
    }
}
